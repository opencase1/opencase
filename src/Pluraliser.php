<?php declare(strict_types=1);

namespace Drupal\opencase;


class Pluraliser {
    const NO_CHANGE = ['Staff'];
    public static function pluralise($text) {
        if (in_array($text, self::NO_CHANGE)) {
            return $text;
        } else {
            return $text . "s";
        }
    }
}