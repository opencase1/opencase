<?php declare(strict_types=1);

namespace Drupal\opencase\EventSubscriber;

use Drupal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Url;

/**
 * Class OpencaseSubscriber.
 *
 * @package Drupal\opencase\EventSubscriber
 */
class OpencaseSubscriber implements EventSubscriberInterface {

    private string $alternativeHomePage = '/opencase/cases/all';

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents(): array
  {
    $events[KernelEvents::REQUEST][] = ['onRequest'];
    return $events;
  }

  /**
   * Manipulates the request object.
   *
   * @param RequestEvent $event
   *   The Event to process.
   */
  public function onRequest(RequestEvent $event): void {
    if ($this->requestIsForHomePage($event) && $this->currentUserShouldBeDirectedToAlternativeHomePage()){
        $this->redirectToAlternativeHomePage($event);
    }
  }

  private function requestIsForHomePage($event):bool {
    return $event->getRequest()->getRequestUri() == '/';
  }

  private function currentUserShouldBeDirectedToAlternativeHomePage():bool {
    return $this->currentUserCanSeeAllCases();
  }
  private function currentUserCanSeeAllCases():bool {
    return Drupal::currentUser()->hasPermission('view published case entities');
  }

  private function redirectToAlternativeHomePage(RequestEvent $event):void {
    $redirect_target_url = Url::fromUserInput($this->alternativeHomePage);
    $response = new RedirectResponse($redirect_target_url->setAbsolute()->toString());
    $event->setResponse($response);
  }
}
