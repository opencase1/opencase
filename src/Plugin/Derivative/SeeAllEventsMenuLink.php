<?php

namespace Drupal\opencase\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *  * Derivative class that provides the menu links adding various types of events
 *  */
class SeeAllEventsMenuLink extends DeriverBase implements ContainerDeriverInterface
{

  /**
   *    * @var EntityTypeManagerInterface $entityTypeManager .
   *    */

  protected $entityTypeManager;

  /**
   *    * Creates a AddActorsMenuLink instance.
   *    *
   *    * @param $base_plugin_id
   *    * @param EntityTypeManagerInterface $entity_type_manager
   *    */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager)
  {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   *    * {@inheritdoc}
   *    */
  public static function create(ContainerInterface $container, $base_plugin_id)
  {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager')
    );
  }

  /**
   *    * {@inheritdoc}
   *    */
  public function getDerivativeDefinitions($base_plugin_definition)
  {
    $links = [];
    $eventTypes = $this->entityTypeManager->getStorage('oc_event_type')->loadMultiple();
    foreach ($eventTypes as $id => $eventType) {
      $links[$id] = [
        'title' => \Drupal\opencase\Pluraliser::pluralise($eventType->label()),
          'route_name' => "view.events.page_1",
          'route_parameters' => ['type' => $eventType->id()]
        ] + $base_plugin_definition;
    }
    return $links;
  }
}
