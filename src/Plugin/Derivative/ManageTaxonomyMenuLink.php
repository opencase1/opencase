<?php

namespace Drupal\opencase\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ManageTaxonomyMenuLink extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var EntityTypeManagerInterface $entityTypeManager.
   */

  protected $entityTypeManager;
  
  /**
   * Creates a AddEventsMenuLink instance.
   *
   * @param $base_plugin_id
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

 /**
   * {@inheritdoc}
   */
 public static function create(ContainerInterface $container, $base_plugin_id) {
   return new static(
    $base_plugin_id,
    $container->get('entity_type.manager')
   );
 }
  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];
    $vocabs = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
    foreach ($vocabs as $id => $vocab) {
      $links[$id] = [
        'title' => 'Manage ' . $vocab->label(),
        'route_name' => "entity.taxonomy_vocabulary.overview_form",
        'route_parameters' => ['taxonomy_vocabulary' => $vocab->id()]
      ] + $base_plugin_definition;
    }
    return $links;
  }
}
