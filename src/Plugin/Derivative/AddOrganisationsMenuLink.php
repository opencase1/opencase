<?php

namespace Drupal\opencase\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derivative class that provides the menu links adding various types of organisations
 */
class AddOrganisationsMenuLink extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var EntityTypeManagerInterface $entityTypeManager.
   */

  protected $entityTypeManager;
  
  /**
   * Creates a AddOrganisationsMenuLink instance.
   *
   * @param $base_plugin_id
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

 /**
   * {@inheritdoc}
   */
 public static function create(ContainerInterface $container, $base_plugin_id) {
   return new static(
    $base_plugin_id,
    $container->get('entity_type.manager')
   );
 }
  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];
    $organisationTypes = $this->entityTypeManager->getStorage('oc_organisation_type')->loadMultiple();
    foreach ($organisationTypes as $id => $organisationType) {
      $links[$id] = [
        'title' => $organisationType->label(),
        'route_name' => "entity.oc_organisation.add_form",
        'route_parameters' => ['oc_organisation_type' => $organisationType->id()]
      ] + $base_plugin_definition;
    }
    return $links;
  }
}
