<?php

namespace Drupal\opencase\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derivative class that provides the menu links adding various types of events
 */
class AddEventsMenuLink extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var EntityTypeManagerInterface $entityTypeManager.
   */

  protected $entityTypeManager;
  
  /**
   * Creates a AddEventsMenuLink instance.
   *
   * @param $base_plugin_id
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

 /**
   * {@inheritdoc}
   */
 public static function create(ContainerInterface $container, $base_plugin_id) {
   return new static(
    $base_plugin_id,
    $container->get('entity_type.manager')
   );
 }
  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];
    $eventTypes = $this->entityTypeManager->getStorage('oc_event_type')->loadMultiple();
    foreach ($eventTypes as $id => $eventType) {
      $links[$id] = [
        'title' => $eventType->label() . " Event",
        'route_name' => "entity.oc_event.add_form",
        'route_parameters' => ['oc_event_type' => $eventType->id()]
      ] + $base_plugin_definition;
    }
    return $links;
  }
}
