<?php declare(strict_types = 1);

namespace Drupal\opencase\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a Block for adding cases to an actor
 *
 * @Block(
 *   id = "add_case",
 *   admin_label = @Translation("Add Case"),
 *   category = @Translation("Opencase"),
 * )
 */
class AddCase extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build():array {
        $actor = \Drupal::routeMatch()->getParameter('oc_actor');
        $target_id = $actor->id();
        $actorType = $actor->bundle();
        $markup = "<ul>";
        $case_types = $this->getCaseTypesToDisplay($actorType);
        foreach($case_types as $id => $info) {
            $label = $info['label'];
            $markup .= "<li><a href='/opencase/oc_case/add/$id?target_id=$target_id&destination=/opencase/oc_actor/$target_id'>$label</a></li>";
        }
        $markup .= "</ul>";
        return array('#markup' => $markup);
    }

    public function getCacheMaxAge():int {
        return 0;
    }

    private function getCaseTypesToDisplay(string $actorType): array {
        // Client modules will provide a list of what case types (bundles) are relevant for each actor type.
        // Check if they are implemented, and if so display them.
        $implemented_case_types = \Drupal::service('entity_type.bundle.info')->getBundleInfo('oc_case');
        $relevant_case_type_ids = \Drupal::moduleHandler()->invokeAll('relevant_case_type_ids', [$actorType]);
        $case_types_to_display = [];
        foreach ($relevant_case_type_ids as $type_id) {
            if (array_key_exists($type_id, $implemented_case_types)) {
                $case_types_to_display[$type_id] = $implemented_case_types[$type_id];
            }
        }
        return $case_types_to_display;
    }
}
