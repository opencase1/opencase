<?php declare(strict_types = 1);

namespace Drupal\opencase\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\opencase_cases\Entity\OCCase;
use Drupal\opencase_entities\Entity\OCActor;

/**
 * Provides a Block with some help text about actor type fields
 *
 * @Block(
 *   id = "add_activity",
 *   admin_label = @Translation("Add Activity"),
 *   category = @Translation("Opencase"),
 * )
 */
class AddActivity extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build():array {
        if ($this->isOnACase()) {
            return $this->makeBlockForAddingActivityToCase();
        } elseif ($this->isOnAnActor()) {
            return $this->makeBlockForAddingActivityToActor();
        }
        return ['#markup' => 'This block should only be placed on a case or on an actor.'];
    }

    private function makeBlockForAddingActivityToCase():array {
        $case = \Drupal::routeMatch()->getParameter('oc_case');
        $case_id = $case->id();
        $markup = "<ul>";
        $bundles = $this->getActivityBundlesFor('oc_case', $case->bundle());
        foreach($bundles as $bundle => $info) {
            $label = $info['label'];
            $markup .= "<li><a href='/opencase/oc_activity/add/$bundle?case_id=$case_id&destination=/opencase/oc_case/$case_id'>$label</a></li>";
        }
        $markup .= "</ul>";
        return array('#markup' => $markup);
    }
    private function makeBlockForAddingActivityToActor():array {
        $actor = \Drupal::routeMatch()->getParameter('oc_actor');
        $target_id = $actor->id();
        $markup = "<ul>";
        $bundles = $this->getActivityBundlesFor('oc_actor', $actor->bundle());
        foreach($bundles as $bundle => $info) {
            $label = $info['label'];
            $markup .= "<li><a href='/opencase/oc_activity/add/$bundle?target_id=$target_id&destination=/opencase/oc_actor/$target_id'>$label</a></li>";
        }
        $markup .= "</ul>";
        return array('#markup' => $markup);
    }

    private function isOnACase():bool {
        return \Drupal::routeMatch()->getParameter('oc_case') instanceof OCCase;
    }

    private function isOnAnActor():bool {
        return \Drupal::routeMatch()->getParameter('oc_actor') instanceof OCActor;
    }


    public function getCacheMaxAge():int {
        return 0;
    }

    private function getActivityBundlesFor(string $entityType, string $bundle): array {
        // Client modules will provide a list of what activity types (bundles) are relevant for each actor type.
        // Check if they are implemented, and if so display them.
        $implemented_activity_types = \Drupal::service('entity_type.bundle.info')->getBundleInfo('oc_activity');
        $relevant_activity_type_ids = \Drupal::moduleHandler()->invokeAll('relevant_activity_type_ids', [$entityType, $bundle]);
        $activity_types_to_display = [];
        foreach ($relevant_activity_type_ids as $type_id) {
            if (array_key_exists($type_id, $implemented_activity_types)) {
                $activity_types_to_display[$type_id] = $implemented_activity_types[$type_id];
            }
        }
        return $activity_types_to_display;
    }
}
