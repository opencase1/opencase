<?php

namespace Drupal\opencase\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;

/**
 * Plugin implementation of the 'entity reference label delta' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_current_target_excluder",
 *   label = @Translation("Exclude current entity from list (ONLY WORKS WITH ORGS FOR NOW)"),
 *   description = @Translation("Don't show the referenced entity if it is the entity being currently viewed"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceCurrentTargetExcluder extends EntityReferenceLabelFormatter {

  // Remove the item which matches the organisation being currently viewed. 
  public function viewElements(\Drupal\Core\Field\FieldItemListInterface $items, $langcode) {
    foreach($items as $delta=>$item) {
      $current_org = \Drupal::routeMatch()->getParameter('oc_organisation')->id();
      $item_target_id = $item->get('target_id')->getValue();
      if ($current_org == $item_target_id) {
        $items->removeItem($delta);
        break;
      }
    }
    $elements = parent::viewElements($items, $langcode);
    return $elements;
  }
  //
}
