<?php
namespace Drupal\opencase\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

/**
 * Represents a menu link for seeing all Events of various types.
 */
class SeeAllEventsMenuLink extends MenuLinkDefault {}
