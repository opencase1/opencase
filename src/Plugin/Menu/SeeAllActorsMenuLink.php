<?php
namespace Drupal\opencase\Plugin\Menu;
 
use Drupal\Core\Menu\MenuLinkDefault;
 
/**
 * Represents a menu link for seeing all actors of various types.
 */
class SeeAllActorsMenuLink extends MenuLinkDefault {}
