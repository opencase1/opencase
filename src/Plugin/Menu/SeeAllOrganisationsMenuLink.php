<?php
namespace Drupal\opencase\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

/**
 * Represents a menu link for seeing all organisations of various types.
 */
class SeeAllOrganisationsMenuLink extends MenuLinkDefault {}
