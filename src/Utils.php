<?php declare(strict_types =1);

namespace Drupal\opencase;

use \Drupal;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

class Utils {

    public function __construct(
        EntityTypeManagerInterface $entityTypeManager = null
    ) {
        if ($entityTypeManager == null) {
            $entityTypeManager = Drupal::entityTypeManager();
        }
        $this->entityTypeManager = $entityTypeManager;
    }

     /**
   * Utility: find term by name and vid.
   *
   * @param string $name
   *   Term name.
   * @param string $vid
   *   Term vid.
   * @return int
   *   Term id, or 0 if none.
   */
    public function getTidByName(string $name, string $vid):int {
        if (empty($name) || empty($vid)) {
            return 0;
        }
        $properties = [
            'name' => $name,
            'vid' => $vid,
        ];
        $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties($properties);
        $term = reset($terms);
        return (int)(!empty($term) ? $term->id() : 0);
    }
}
