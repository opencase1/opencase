<?php

namespace Drupal\opencase\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class OCEventForm.
 */
class OCEventForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $oc_event = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $oc_event->label(),
      '#description' => $this->t("Label for the Event."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $oc_event->id(),
      '#machine_name' => [
        'exists' => '\Drupal\opencase\Entity\OCEvent::load',
      ],
      '#disabled' => !$oc_event->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $oc_event = $this->entity;
    $status = $oc_event->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Event.', [
          '%label' => $oc_event->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Event.', [
          '%label' => $oc_event->label(),
        ]));
    }
    $form_state->setRedirectUrl($oc_event->toUrl('collection'));
  }

}
