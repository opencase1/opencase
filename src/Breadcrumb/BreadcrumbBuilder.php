<?php  declare(strict_types = 1);

namespace Drupal\opencase\Breadcrumb;
 
use Drupal\Core\Link ;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\opencase_cases\Entity\OCCase;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\opencase_entities\Entity\OCOrganisation;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;

class BreadcrumbBuilder implements BreadcrumbBuilderInterface {

  private string $title;
  private function addTitle($route_match) {
    $title = $this->title ?? $this->getPageTitle($route_match);
    $this->breadcrumb->addLink(Link::createFromRoute($title, '<none>'));
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
      return TRUE;
  }

  private function getPageTitle($route_match) {
    $request = \Drupal::request();
    return \Drupal::service('title_resolver')->getTitle($request, $route_match->getRouteObject());
  }

  // This has to cope with being given null because some cases have no
  // target, i.e. if the target has been deleted but the case intentionally
  // preserved.
  private function addLinksForCaseTarget(?ContentEntityBase $entity) {
    if ($entity instanceof OCOrganisation) {
      $this->addLinksForOrganisation($entity);
    }
  }

  private function addLinksForOrganisation(ContentEntityBase $entity) {
    $fields = ['field_umbrella_client'];
    $this->addLinks($entity, $fields);
  }

  private function addLinksForCase(OCCase $case) {
    $fields = ['client', 'field_project'];
    $this->addLinksForCaseTarget($case->getTargetEntity());
    $this->addLinks($case, $fields);
  }

  private function addLinks(ContentEntityBase $entity, array $fields) {
    foreach ($fields as $field) {
      if ($entity->hasField($field) && !$entity->get($field)->isEmpty() && !is_null($entity->$field->entity)) {
        $this->breadcrumb->addLink($entity->$field->entity->toLink());
      }
    } 
  }
 
  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $this->breadcrumb = new Breadcrumb();
    $this->breadcrumb->addLink(Link::createFromRoute('Home', '<front>'));
    $params = $route_match->getParameters();

    if ($params->has('oc_activity')) {
      $activity = $params->get('oc_activity');
      if (!$activity->get('oc_case')->isEmpty()){
        $this->addLinksForCase($activity->get('oc_case')->entity);
      } 
      $this->addLinks($activity, ['client', 'oc_case']);
      $this->title = $activity->getName() ?? $activity->type->entity->label();
    }

    elseif ($params->has('oc_organisation')) {
      $this->addLinksForOrganisation($params->get('oc_organisation'));
    }

    elseif ($params->has('oc_case')) {
      $this->addLinksForCase($params->get('oc_case'));
    }

    elseif ($params->has('oc_case_fee')) {
      $case_provision = $params->get('oc_case_fee');
      $this->addLinksForCase($case_provision->get('oc_case')->entity);
      $this->addLinks($case_provision, ['oc_case']);
      $this->title = "Fee";
    }

    elseif ($params->has('oc_case_provision')) {
      $case_provision = $params->get('oc_case_provision');
      $this->addLinksForCase($case_provision->get('oc_case')->entity);
      $this->addLinks($case_provision, ['oc_case']);
      $this->title = $case_provision->type->entity->label();
    }

    $this->addTitle($route_match);

    // Don't forget to add cache control by a route.
    // Otherwise all pages will have the same breadcrumb.
    $this->breadcrumb->addCacheContexts(['route']);
    return $this->breadcrumb;
  }
}
