<?php declare(strict_types = 1);

namespace Drupal\Tests\opencase\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\Core\Form\FormState;
use Drupal\Tests\opencase\Unit\EntityTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\opencase_cases\Form\OCCaseRevisionRevertForm;
use Drupal\opencase_entities\Form\OCActorRevisionRevertForm;
use Drupal\opencase_entities\Form\OCActivityRevisionRevertForm;
use Drupal\opencase_entities\Form\OCOrganisationRevisionRevertForm;

class RevisionRevertFormTest extends UnitTestCase{

    use EntityTrait;

    public function setUp(): void {
        $entityTypeManager = $this->getEntityTypeManager();
        $storage = $this->getStorage($entityTypeManager);
        $dateFormatter = $this->getMockBuilder('\\Drupal\\Core\\Datetime\\DateFormatterInterface')->disableOriginalConstructor()->getMock();
        $revision = $this->getMockBuilder('\\Drupal\\opencase_entities\\Entity\OCActivity')->disableOriginalConstructor()->getMock();
        $request = new Request([], [], [], [], [], [], [], json_encode([
            'foo' => 'bar'
        ]));
        $requestStack = new RequestStack();
        $requestStack->push($request);
        $dateFormatter->method('format');
        $storage->method('loadRevision')->willReturn($revision);
        $this->container = $this->getContainer([
            'entity_type.manager'=> $entityTypeManager,
            'date.formatter' => $dateFormatter,
            'string_translation'=> self::getStringTranslationStub(),
            'request_stack'=> $requestStack
        ]);

    }

    public function testBuildFormForRevertingActivity():void {
        $this->reverter = OCActivityRevisionRevertForm::create($this->container);
        $form = [];
        $this->assertTrue(is_array($this->reverter->buildForm($form, new FormState())));
    }
    public function testBuildFormForRevertingActor():void {
        $this->reverter = OCActorRevisionRevertForm::create($this->container);
        $form = [];
        $this->assertTrue(is_array($this->reverter->buildForm($form, new FormState())));
    }
    public function testBuildFormForRevertingCase():void {
        $this->reverter = OCCaseRevisionRevertForm::create($this->container);
        $form = [];
        $this->assertTrue(is_array($this->reverter->buildForm($form, new FormState())));
    }
    public function testBuildFormForRevertingOrganisation():void {
        $this->reverter = OCOrganisationRevisionRevertForm::create($this->container);
        $form = [];
        $this->assertTrue(is_array($this->reverter->buildForm($form, new FormState())));
    }
}
