<?php 
namespace Drupal\Tests\opencase\Unit;

use PHPUnit\Framework\MockObject\MockObject;
use Drupal\Core\DependencyInjection\ContainerBuilder;

trait EntityTrait {
    public function getEntityTypeManager() {
        return $this->getMockBuilder('\\Drupal\\Core\\Entity\\EntityTypeManager')->disableOriginalConstructor()->getMock();
    }

    public function getStorage(MockObject $entityTypeManager, string $entityTypeToExpect = ''): MockObject  {
        $storage = $this->getMockBuilder('\\Drupal\\Core\\Entity\\EntityStorageInterface')->disableOriginalConstructor()->getMock(); 
        $entityTypeManager->method('getStorage')->willReturn($storage);  
        if ($entityTypeToExpect) {
            $entityTypeManager->expects($this->any())->method('getStorage')->with($entityTypeToExpect)->willReturn($storage);  
        }
        return $storage; 
    }

    public function getQuery(MockObject $storage): MockObject {
        $query = $this->getMockBuilder('\\Drupal\\Core\\Entity\\Query\\QueryInterface')->getMock();
        $storage->method('getQuery')->willReturn($query);
        return $query;
    }

    public function getEntity(): MockObject {
        return $this->getMockBuilder('\\Drupal\\Core\\Entity\\EntityBase')->disableOriginalConstructor()->getMock();
    }
    public function getContainer(array $services): ContainerBuilder {
        $container = new ContainerBuilder();
        foreach ($services as $key => $mock) {
            $container->set($key, $mock);
        }
        \Drupal::setContainer($container);
        return $container;
    }
}