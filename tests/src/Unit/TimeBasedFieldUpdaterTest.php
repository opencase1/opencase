<?php declare(strict_types = 1);

namespace Drupal\Tests\opencase\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\opencase\TimeBasedFieldUpdater;

class TimeBasedFieldUpdaterTest extends UnitTestCase{

    use EntityTrait;

    function setUp():void {
        $this->etm = $this->getEntityTypeManager();
        $this->storage = $this->getStorage($this->etm);
        $this->query = $this->getQuery($this->storage);
        $this->updater = new TimeBasedFieldUpdater($this->etm, 'dummy_entity_type', 'dummy_bundle', 'dummy_date_field');

    }
    function testFieldIsUpdatedOnEntityReturnedByQuery():void {
        $this->query->method('execute')->willReturn(['1']);
        $this->entity = $this->getMockBuilder('\\Drupal\\Core\\Entity\\EntityBase')->disableOriginalConstructor()->getMock();
        $this->storage->expects($this->once())->method('load')->with(1)->willReturn($this->entity);
        $this->updater->update([], '3 months', ['dummy_field' => 4]);
        $this->assertEquals($this->entity->dummy_field, 4);
    }
    function testFieldIsUpdatedOnAllEntitiesReturnedByQuery():void {
        $this->query->method('execute')->willReturn(['1', '2']);
        $entity = $this->getEntity();
        $entity2 = $this->getEntity();
        $this->storage->method('load')->willReturnMap([['1', $entity], ['2', $entity2]]);
        $this->updater->update([], '3 months', ['dummy_field' => 4]);
        $this->assertEquals($entity->dummy_field, 4);
        $this->assertEquals($entity2->dummy_field, 4);
    }

    function testMultipleFieldsAreUpdated(): void {
        $this->query->method('execute')->willReturn(['1']);
        $entity = $this->getEntity();       
        $this->storage->expects($this->once())->method('load')->with(1)->willReturn($entity);
        $this->updater->update([], '3 months', ['dummy_field' => 4, 'dummy_field_2' => 5]);
        $this->assertEquals($entity->dummy_field, 4);
        $this->assertEquals($entity->dummy_field_2, 5);

    }

    function testBundleAndDateAndExtraConditionsAreAllAddedAsQueryConditions(): void {
        $this->query->method('execute')->willReturn([]);

        $this->query->expects($this->exactly(4))->method('condition')->withConsecutive(
          ['dummy_field', 'dummy_value', '<'],
          ['dummy_field_2', 'dummy_value_2', '='],
          ['dummy_date_field', date('Y-m-d', strtotime('- 3 months')), "<"],
          ['type', 'dummy_bundle', '=']);

        $this->updater->update([['dummy_field', 'dummy_value', '<'], ['dummy_field_2', 'dummy_value_2', '='] ], '3 months', ['dummy_field' => 4]);
    }
}
