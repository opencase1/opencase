<?php declare(strict_types = 1);

namespace Drupal\Tests\opencase\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\opencase\Utils;

class UtilsTest extends UnitTestCase{
    public function setUp(): void {
        /** @var \Drupal\core\Entity\EntityTypeManager&\PHPUnit\Framework\MockObject\MockObject $entityTypeManager */
        $this->entityTypeManager = $this->getMockBuilder('\\Drupal\\Core\\Entity\\EntityTypeManager')->disableOriginalConstructor()->getMock();
        $this->utils = new Utils($this->entityTypeManager);
        $this->storage = $this->getMockBuilder('\\Drupal\\Core\\Entity\\EntityStorageInterface')->getMock();
        $this->query = $this->getMockBuilder('\\Drupal\\Core\\Entity\\Query\\QueryInterface')->getMock();
        $this->entityTypeManager->method('getStorage')->willReturn($this->storage);
    }

    public function testGetTidByNameGetsTid():void {
        $term_entity = $this->getMockBuilder('\\Drupal\\Core\\Entity\\EntityBase')->disableOriginalConstructor()->getMock();
        $term_entity->expects($this->once())->method('id')->willReturn('3');
        $this->storage->expects($this->once())->method('loadByProperties')->with(['name' => 'foo', 'vid' => 'bar'])
            ->willReturn([$term_entity]);
        $this->assertEquals($this->utils->getTidByName('foo', 'bar'), 3);
    }
}
