<?php declare(strict_types = 1);

namespace Drupal\Tests\opencase\Unit;

use Drupal\Tests\UnitTestCase;

class OCActorTest extends UnitTestCase{

    use EntityTrait;

    public function setUp(): void {
        $this->etm = $this->getEntityTypeManager();
        $this->getContainer([
            'entity_type.manager' => $this->etm
        ]);
    }

    public function testGetCountOfCaseProvisionsWithNoExtraConditions(): void{  
        $storage = $this->getStorage($this->etm, 'oc_case_provision');
        $query = $this->getQuery($storage); 
        $actor = $this->getMockBuilder('\\Drupal\\opencase_entities\\Entity\\OCActor')->disableOriginalConstructor()
            ->onlyMethods(['id'])
            ->getMock();
        $actor->expects($this->once())->method('id')->willReturn(5);
        $query->expects($this->once())->method('condition')->with('oc_provider', 5);
        $query->expects($this->once())->method('execute')->willReturn([1,2,3,4]);
        $count = $actor->getCountOfCaseProvisions();
        $this->assertTrue($count == 4);
    }


    public function testGetCountOfCaseProvisionsWithExtraConditions(): void{  
        $storage = $this->getStorage($this->etm, 'oc_case_provision');
        $query = $this->getQuery($storage); 
        $actor = $this->getMockBuilder('\\Drupal\\opencase_entities\\Entity\\OCActor')->disableOriginalConstructor()
            ->onlyMethods(['id'])
            ->getMock();
        $actor->expects($this->once())->method('id')->willReturn(5);        
        $query->expects($this->exactly(2))->method('condition')->withConsecutive(
            ['oc_provider', 5],
            ['some_date_field', '2022-01-01', '<']);
        $query->expects($this->once())->method('execute')->willReturn([1,2,3,4]);
        $count = $actor->getCountOfCaseProvisions([['some_date_field', '2022-01-01', '<']]);
        $this->assertTrue($count == 4);
    }

    public function testGetCountOfCaseProvisionsWithExtraConditionsWithAssumedEqualityOperator(): void{  
        $storage = $this->getStorage($this->etm, 'oc_case_provision');
        $query = $this->getQuery($storage); 
        $actor = $this->getMockBuilder('\\Drupal\\opencase_entities\\Entity\\OCActor')->disableOriginalConstructor()
            ->onlyMethods(['id'])
            ->getMock();
        $actor->expects($this->once())->method('id')->willReturn(5);        
        $query->expects($this->exactly(2))->method('condition')->withConsecutive(
            ['oc_provider', 5],
            ['some_date_field', '2022-01-01', '=']);
        $query->expects($this->once())->method('execute')->willReturn([1,2,3,4]);
        $count = $actor->getCountOfCaseProvisions([['some_date_field', '2022-01-01']]);
        $this->assertTrue($count == 4);
    }
}
