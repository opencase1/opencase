<?php declare(strict_types = 1);

namespace Drupal\Tests\opencase\Unit;

use Drupal\Tests\UnitTestCase;

class OCCaseTest extends UnitTestCase{

    use EntityTrait;

    private $case;

    public function setUp(): void {
        $this->etm = $this->getEntityTypeManager();
        $this->getContainer([
            'entity_type.manager' => $this->etm
        ]);
        $this->case = $this->getMockBuilder('\\Drupal\\opencase_cases\\Entity\\OCCase')->disableOriginalConstructor()
            ->onlyMethods(['id'])
            ->getMock();
    }
   
    public function testGetCaseProviderIds_SingleCaseProvision(): void{  
        // It will first get the id of the case provision that references this case
        $storage = $this->getStorage($this->etm, 'oc_case_provision');
        $query = $this->getQuery($storage); 
        $this->case->expects($this->once())->method('id')->willReturn(5);        
        $query->expects($this->once())->method('condition')->with('oc_case.target_id', 5);
        $query->expects($this->once())->method('execute')->willReturn([1]);

        // Then it will load the provision
        $provision = $this->getMockBuilder('\\Drupal\\opencase_cases\\Entity\\OCCaseProvision')->disableOriginalConstructor()->getMock();
        $storage->expects($this->once())->method('load')->with(1)->willReturn($provision);      

        // Then it will get the target id of the provider field from the provision and return it as an array
        $providerField = $this->getMockBuilder('\\Drupal\\COre\\Field\\FieldItemListInterface')->disableOriginalConstructor()->getMock();
        $providerField->target_id = '45';
        $provision->expects($this->once())->method('get')->with('oc_provider')->willReturn($providerField);

        $ids = $this->case->getCaseProviderIds();
        $this->assertTrue($ids == [45]);
    }

       
    public function testGetCaseProviderIds_MultipleCaseProvisions(): void{  
        // It will first get the id of the case provisions that reference this case
        $storage = $this->getStorage($this->etm, 'oc_case_provision');
        $query = $this->getQuery($storage); 
        $this->case->expects($this->once())->method('id')->willReturn(5);        
        $query->expects($this->once())->method('condition')->with('oc_case.target_id', 5);
        $query->expects($this->once())->method('execute')->willReturn([1, 2, 3]);

        // Then it will load the provisions
        $provision1 = $this->getMockBuilder('\\Drupal\\opencase_cases\\Entity\\OCCaseProvision')->disableOriginalConstructor()->getMock();
        $provision2 = $this->getMockBuilder('\\Drupal\\opencase_cases\\Entity\\OCCaseProvision')->disableOriginalConstructor()->getMock();
        $provision3 = $this->getMockBuilder('\\Drupal\\opencase_cases\\Entity\\OCCaseProvision')->disableOriginalConstructor()->getMock();
        $storage->method('load')->willReturnMap([[1, $provision1], [2, $provision2], [3, $provision3]]);      

        // Then it will get the target id of the provider field from each provision and return them as an array
        $providerField1 = $this->getMockBuilder('\\Drupal\\COre\\Field\\FieldItemListInterface')->disableOriginalConstructor()->getMock();
        $providerField1->target_id = '45';
        $provision1->expects($this->once())->method('get')->with('oc_provider')->willReturn($providerField1);
        $providerField2 = $this->getMockBuilder('\\Drupal\\COre\\Field\\FieldItemListInterface')->disableOriginalConstructor()->getMock();
        $providerField2->target_id = '55';
        $provision2->expects($this->once())->method('get')->with('oc_provider')->willReturn($providerField2);
        $providerField3 = $this->getMockBuilder('\\Drupal\\COre\\Field\\FieldItemListInterface')->disableOriginalConstructor()->getMock();
        $providerField3->target_id = '65';
        $provision3->expects($this->once())->method('get')->with('oc_provider')->willReturn($providerField3);

        $ids = $this->case->getCaseProviderIds();
        $this->assertTrue($ids == [45, 55, 65]);
    }

    public function testGetCaseProviderIds_SingleCaseProvision_RoleSpecifiedAndMatched(): void{  
        // It will first get the id of the case provision that references this case
        $storage = $this->getStorage($this->etm, 'oc_case_provision');
        $query = $this->getQuery($storage); 
        $this->case->expects($this->once())->method('id')->willReturn(5);        
        $query->expects($this->once())->method('condition')->with('oc_case.target_id', 5);
        $query->expects($this->once())->method('execute')->willReturn([1]);

        // Then it will load the provision
        $provision = $this->getMockBuilder('\\Drupal\\opencase_cases\\Entity\\OCCaseProvision')->disableOriginalConstructor()->getMock();
        $storage->expects($this->once())->method('load')->with(1)->willReturn($provision);      

        // Then it will check the role field, and find that the role is the one specified
        // so it will get the target id of the provider field from the provision and return it as an array
        $roleField = $this->getMockBuilder('\\Drupal\\COre\\Field\\FieldItemListInterface')->disableOriginalConstructor()->getMock();
        $roleField->target_id = '7';        
        $providerField = $this->getMockBuilder('\\Drupal\\COre\\Field\\FieldItemListInterface')->disableOriginalConstructor()->getMock();
        $providerField->target_id = '45';
        $provision->expects($this->any())->method('get')->withConsecutive(['oc_case_provider_role'], ['oc_provider'])->willReturnMap(
            [['oc_provider', $providerField], ['oc_case_provider_role', $roleField]]
        );

        $ids = $this->case->getCaseProviderIds(7);
        $this->assertTrue($ids == [45]);
    }

    public function testGetCaseProviderIds_SingleCaseProvision_RoleSpecifiedAndNotMatched(): void{  
        // It will first get the id of the case provision that references this case
        $storage = $this->getStorage($this->etm, 'oc_case_provision');
        $query = $this->getQuery($storage); 
        $this->case->expects($this->once())->method('id')->willReturn(5);        
        $query->expects($this->once())->method('condition')->with('oc_case.target_id', 5);
        $query->expects($this->once())->method('execute')->willReturn([1]);

        // Then it will load the provision
        $provision = $this->getMockBuilder('\\Drupal\\opencase_cases\\Entity\\OCCaseProvision')->disableOriginalConstructor()->getMock();
        $storage->expects($this->once())->method('load')->with(1)->willReturn($provision);      

        // Then it will check the role field, and find that the role is *not* the one specified
        // so it will return nothing
        $roleField = $this->getMockBuilder('\\Drupal\\COre\\Field\\FieldItemListInterface')->disableOriginalConstructor()->getMock();
        $roleField->target_id = '7';        
        $providerField = $this->getMockBuilder('\\Drupal\\COre\\Field\\FieldItemListInterface')->disableOriginalConstructor()->getMock();
        $providerField->target_id = '45';
        $provision->expects($this->any())->method('get')->withConsecutive(['oc_case_provider_role'], ['oc_provider'])->willReturnMap(
            [['oc_provider', $providerField], ['oc_case_provider_role', $roleField]]
        );

        $ids = $this->case->getCaseProviderIds(8);
        $this->assertTrue($ids == []);
    }
}
