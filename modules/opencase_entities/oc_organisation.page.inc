<?php

/**
 * @file
 * Contains oc_organisation.page.inc.
 *
 * Page callback for Organisation entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Organisation templates.
 *
 * Default template: oc_organisation.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_oc_organisation(array &$variables) {
_template_preprocess_entity($variables);
}
