<?php

/**
 * @file
 * Contains oc_equal_opps.page.inc.
 *
 * Page callback for Equal Opps entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Equal Opps templates.
 *
 * Default template: oc_equal_opps.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_oc_equal_opps(array &$variables) {
  // Fetch OCEqualOpps Entity Object.
  $oc_equal_opps = $variables['elements']['#oc_equal_opps'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
