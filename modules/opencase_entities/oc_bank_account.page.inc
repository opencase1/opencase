<?php

/**
 * @file
 * Contains oc_bank_account.page.inc.
 *
 * Page callback for Bank Account entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Bank Account templates.
 *
 * Default template: oc_bank_account.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_oc_bank_account(array &$variables) {
  // Fetch OCBankAccount Entity Object.
  $oc_bank_account = $variables['elements']['#oc_bank_account'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
