<?php

/**
 * @file
 * Contains oc_organisation_relation.page.inc.
 *
 * Page callback for Organisation Relation entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Organisation Relation templates.
 *
 * Default template: oc_organisation_relation.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_oc_organisation_relation(array &$variables) {
  // Fetch OCOrganisationRelation Entity Object.
  $oc_organisation_relation = $variables['elements']['#oc_organisation_relation'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
