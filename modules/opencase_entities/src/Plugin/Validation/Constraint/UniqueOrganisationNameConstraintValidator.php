<?php

namespace Drupal\opencase_entities\Plugin\Validation\Constraint;

use Drupal\Core\Field\FieldItemList;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueOrganisationNameConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   *
   * Invalidates a create form entry if the name is already in use.
   * Invalidates an edit form entry if the name is already in use by a different org.
   */
  public function validate($fieldItemList, Constraint $constraint) {
    // The method signature had to match the one in the parent class,
    // but we expect a FieldItemList.
    if (!($fieldItemList instanceof FieldItemList)) {
      return;
    }

    $id = $fieldItemList->getEntity()->id();
    $editing = isset($id);

    foreach ($fieldItemList as $item) {
      $name = $item->value;
      if ($editing) {
        // Invalidate only if a *different* org already exists with same name.
        $query = \Drupal::entityQuery('oc_organisation')
          ->accessCheck(FALSE)
          ->condition('name', $name)
          ->condition('id', $id, '!=');
      } else {
        $query = \Drupal::entityQuery('oc_organisation')
          ->accessCheck(FALSE)   
          ->condition('name', $name);
      }

      if (count($query->execute())) {
        $this->context->addViolation($constraint->notUnique, ['%name' => $name]);
      }
    }
  }
}
