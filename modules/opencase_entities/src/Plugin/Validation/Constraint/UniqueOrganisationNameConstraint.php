<?php

namespace Drupal\opencase_entities\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a unique integer.
 *
 * @Constraint(
 *   id = "UniqueOrganisationName",
 *   label = @Translation("Unique Organisation Name", context = "Validation"),
 *   type = "string"
 * )
 */
class UniqueOrganisationNameConstraint extends Constraint {
  public $notUnique = "An organisation with the name %name already exists";

}
