<?php

namespace Drupal\opencase_entities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Bank Account entities.
 *
 * @ingroup opencase_entities
 */
class OCBankAccountListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Bank Account ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\opencase_entities\Entity\OCBankAccount $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.oc_bank_account.edit_form',
      ['oc_bank_account' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
