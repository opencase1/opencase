<?php

namespace Drupal\opencase_entities;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\opencase_entities\Entity\OCOrganisationInterface;

/**
 * Defines the storage handler class for Organisation entities.
 *
 * This extends the base storage class, adding required special handling for
 * Organisation entities.
 *
 * @ingroup opencase_entities
 */
class OCOrganisationStorage extends SqlContentEntityStorage implements OCOrganisationStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(OCOrganisationInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {oc_organisation_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {oc_organisation_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(OCOrganisationInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {oc_organisation_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('oc_organisation_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
