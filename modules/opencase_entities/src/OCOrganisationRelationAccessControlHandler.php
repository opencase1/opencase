<?php

namespace Drupal\opencase_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Organisation Relation entity.
 *
 * @see \Drupal\opencase_entities\Entity\OCOrganisationRelation.
 */
class OCOrganisationRelationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\opencase_entities\Entity\OCOrganisationRelationInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished organisation relation entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published organisation relation entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit organisation relation entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete organisation relation entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add organisation relation entities');
  }


}
