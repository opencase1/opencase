<?php

namespace Drupal\opencase_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Equal Opps entity.
 *
 * @see \Drupal\opencase_entities\Entity\OCEqualOpps.
 */
class OCEqualOppsAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\opencase_entities\Entity\OCEqualOppsInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished equal opps entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published equal opps entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit equal opps entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete equal opps entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add equal opps entities');
  }


}
