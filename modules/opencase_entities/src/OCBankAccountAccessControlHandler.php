<?php

namespace Drupal\opencase_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Bank Account entity.
 *
 * @see \Drupal\opencase_entities\Entity\OCBankAccount.
 */
class OCBankAccountAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\opencase_entities\Entity\OCBankAccountInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished bank account entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published bank account entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit bank account entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete bank account entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add bank account entities');
  }


}
