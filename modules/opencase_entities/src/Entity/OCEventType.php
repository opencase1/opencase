<?php

namespace Drupal\opencase_entities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Event type entity.
 *
 * @ConfigEntityType(
 *   id = "oc_event_type",
 *   label = @Translation("Event type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\opencase_entities\OCEventTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\opencase_entities\Form\OCEventTypeForm",
 *       "edit" = "Drupal\opencase_entities\Form\OCEventTypeForm",
 *       "delete" = "Drupal\opencase_entities\Form\OCEventTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\opencase_entities\OCEventTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "oc_event_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "oc_event",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/opencase/oc_event_type/{oc_event_type}",
 *     "add-form" = "/admin/opencase/oc_event_type/add",
 *     "edit-form" = "/admin/opencase/oc_event_type/{oc_event_type}/edit",
 *     "delete-form" = "/admin/opencase/oc_event_type/{oc_event_type}/delete",
 *     "collection" = "/admin/opencase/oc_event_type"
 *   }
 * )
 */
class OCEventType extends ConfigEntityBundleBase implements OCEventTypeInterface {

  /**
   * The Event type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Event type label.
   *
   * @var string
   */
  protected $label;

}
