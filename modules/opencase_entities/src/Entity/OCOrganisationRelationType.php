<?php

namespace Drupal\opencase_entities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Organisation Relation type entity.
 *
 * @ConfigEntityType(
 *   id = "oc_organisation_relation_type",
 *   label = @Translation("Organisation Relation type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\opencase_entities\OCOrganisationRelationTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\opencase_entities\Form\OCOrganisationRelationTypeForm",
 *       "edit" = "Drupal\opencase_entities\Form\OCOrganisationRelationTypeForm",
 *       "delete" = "Drupal\opencase_entities\Form\OCOrganisationRelationTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\opencase_entities\OCOrganisationRelationTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "oc_organisation_relation_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "oc_organisation_relation",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/opencase/oc_organisation_relation_type/{oc_organisation_relation_type}",
 *     "add-form" = "/admin/opencase/oc_organisation_relation_type/add",
 *     "edit-form" = "/admin/opencase/oc_organisation_relation_type/{oc_organisation_relation_type}/edit",
 *     "delete-form" = "/admin/opencase/oc_organisation_relation_type/{oc_organisation_relation_type}/delete",
 *     "collection" = "/admin/opencase/oc_organisation_relation_type"
 *   }
 * )
 */
class OCOrganisationRelationType extends ConfigEntityBundleBase implements OCOrganisationRelationTypeInterface {

  /**
   * The Organisation Relation type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Organisation Relation type label.
   *
   * @var string
   */
  protected $label;

}
