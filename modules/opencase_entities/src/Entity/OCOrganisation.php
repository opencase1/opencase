<?php

namespace Drupal\opencase_entities\Entity;

use Drupal\user\UserInterface;
use Drupal\opencase_entities\Counties;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\RevisionLogEntityTrait;
use Drupal\Core\Entity\EditorialContentEntityBase;


/**
 * Defines the Organisation entity.
 *
 * @ingroup opencase_entities
 *
 * @ContentEntityType(
 *   id = "oc_organisation",
 *   label = @Translation("Organisation"),
 *   bundle_label = @Translation("Organisation type"),
 *   handlers = {
 *     "storage" = "Drupal\opencase_entities\OCOrganisationStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\opencase_entities\OCOrganisationListBuilder",
 *     "views_data" = "Drupal\opencase_entities\Entity\OCOrganisationViewsData",
 *     "translation" = "Drupal\opencase_entities\OCOrganisationTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\opencase_entities\Form\OCOrganisationForm",
 *       "add" = "Drupal\opencase_entities\Form\OCOrganisationForm",
 *       "edit" = "Drupal\opencase_entities\Form\OCOrganisationForm",
 *       "delete" = "Drupal\opencase_entities\Form\OCOrganisationDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\opencase_entities\OCOrganisationHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\opencase_entities\OCOrganisationAccessControlHandler",
 *   },
 *   base_table = "oc_organisation",
 *   data_table = "oc_organisation_field_data",
 *   revision_table = "oc_organisation_revision",
 *   revision_data_table = "oc_organisation_field_revision",
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log"
 *   },
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer organisation entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/opencase/oc_organisation/{oc_organisation}",
 *     "add-page" = "/opencase/oc_organisation/add",
 *     "add-form" = "/opencase/oc_organisation/add/{oc_organisation_type}",
 *     "edit-form" = "/opencase/oc_organisation/{oc_organisation}/edit",
 *     "delete-form" = "/opencase/oc_organisation/{oc_organisation}/delete",
 *     "version-history" = "/opencase/oc_organisation/{oc_organisation}/revisions",
 *     "revision" = "/opencase/oc_organisation/{oc_organisation}/revisions/{oc_organisation_revision}/view",
 *     "revision_revert" = "/opencase/oc_organisation/{oc_organisation}/revisions/{oc_organisation_revision}/revert",
 *     "revision_delete" = "/opencase/oc_organisation/{oc_organisation}/revisions/{oc_organisation_revision}/delete",
 *     "translation_revert" = "/opencase/oc_organisation/{oc_organisation}/revisions/{oc_organisation_revision}/revert/{langcode}",
 *     "collection" = "/opencase/oc_organisation",
 *   },
 *   bundle_entity_type = "oc_organisation_type",
 *   field_ui_base_route = "entity.oc_organisation_type.edit_form"
 * )
 */
class OCOrganisation extends EditorialContentEntityBase implements OCOrganisationInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;
  use RevisionLogEntityTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the oc_organisation owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['notes'] = BaseFieldDefinition::create('string_long')
      ->setRevisionable(TRUE)
      ->setLabel(t('Message'))
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ));

    $fields['org_website'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Website'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'text_processing' => 0,
        'title' => 0,
        'link_type' => 16
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setRequired(FALSE);

    $fields['contact_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Contact Name'))
      ->setDescription(t('Name of the main contact for this organisation.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setRequired(FALSE);

    $fields['contact_role'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Contact Role'))
      ->setDescription(t('Role of the contact within the organisation'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setRequired(FALSE);

    $fields['email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email Address'))
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);

    $fields['phone'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Phone Number'))
      ->setRevisionable(TRUE)
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 20,
        'text_processing' => 0,
      ))
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);
    $fields['postal_address'] = BaseFieldDefinition::create('string_long')
      ->setRevisionable(TRUE)
      ->setLabel(t('Address'))
      ->setDescription(t('Building name/number and street name'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);
      $fields['city'] = BaseFieldDefinition::create('string')

        ->setRevisionable(TRUE)
        ->setLabel(t('City'))
        ->setDisplayConfigurable('form', true)
        ->setDisplayConfigurable('view', true);
        $fields['county'] = BaseFieldDefinition::create('list_string')
          ->setRevisionable(TRUE)
          ->setLabel(t('County'))
          ->setSettings([
            'allowed_values' => Counties::$counties
          ])
          ->setDisplayConfigurable('form', true)
          ->setDisplayConfigurable('view', true);
          $fields['country'] = BaseFieldDefinition::create('string')
            ->setRevisionable(TRUE)
            ->setLabel(t('Country'))
            ->setSettings(array(
              'default_value' => 'UK'
            ))
            ->setDisplayConfigurable('form', true)
            ->setDisplayConfigurable('view', true);
    $fields['post_code'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Post Code'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 10,
        'text_processing' => 0,
      ))
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);
    $fields['billing_contact_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Billing Contact Name'))
      ->setDescription(t('Name of the main contact for this client.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setRequired(FALSE);

    $fields['billing_email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Billing Email Address'))
      ->setRevisionable(TRUE)
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 100,
        'text_processing' => 0,
      ))
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);

    $fields['billing_phone'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Billing Phone Number'))
      ->setRevisionable(TRUE)
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 20,
        'text_processing' => 0,
      ))
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);
    $fields['billing_postal_address'] = BaseFieldDefinition::create('string_long')
      ->setRevisionable(TRUE)
      ->setLabel(t('Billing Postal Address'))
      ->setDescription(t('Full address, apart from post code.'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);
    $fields['billing_post_code'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Billing Post Code'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 10,
        'text_processing' => 0,
      ))
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Added by'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->addConstraint('UniqueOrganisationName')
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Visible'))
      ->setDescription('If this box is not ticked this record will be hidden from view for most users. Users with access to unpublished entities will be able to restore it if needed.')
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setDefaultValue(TRUE);


    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['files'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Files'))
      ->setDescription(t('Files relating to this organisation'))
      ->setSetting('file_directory', '[date:custom:Y]-[date:custom:m]')
      ->setSetting('handler', 'default:file')
      ->setSetting('file_extensions', 'txt jpg jpeg gif rtf xls xlsx doc swf png pdf docx csv')
      ->setSetting('description_field', 'true')
      ->setSetting('uri_scheme', 'private')
      ->setCardinality(-1)
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
        'weight' => 61,
        'settings' => [
          'progress_indicator' => 'throbber',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'settings' => ['use_description_as_link_text' => 'true']
      ]);
    return $fields;
  }

}
