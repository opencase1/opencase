<?php

namespace Drupal\opencase_entities\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Equal Opps entities.
 *
 * @ingroup opencase_entities
 */
interface OCEqualOppsInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Equal Opps name.
   *
   * @return string
   *   Name of the Equal Opps.
   */
  public function getName();

  /**
   * Sets the Equal Opps name.
   *
   * @param string $name
   *   The Equal Opps name.
   *
   * @return \Drupal\opencase_entities\Entity\OCEqualOppsInterface
   *   The called Equal Opps entity.
   */
  public function setName($name);

  /**
   * Gets the Equal Opps creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Equal Opps.
   */
  public function getCreatedTime();

  /**
   * Sets the Equal Opps creation timestamp.
   *
   * @param int $timestamp
   *   The Equal Opps creation timestamp.
   *
   * @return \Drupal\opencase_entities\Entity\OCEqualOppsInterface
   *   The called Equal Opps entity.
   */
  public function setCreatedTime($timestamp);

}
