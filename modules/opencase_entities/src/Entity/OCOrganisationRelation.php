<?php

namespace Drupal\opencase_entities\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Organisation Relation entity.
 *
 * @ingroup opencase_entities
 *
 * @ContentEntityType(
 *   id = "oc_organisation_relation",
 *   label = @Translation("Organisation Relation"),
 *   bundle_label = @Translation("Organisation Relation type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\opencase_entities\OCOrganisationRelationListBuilder",
 *     "views_data" = "Drupal\opencase_entities\Entity\OCOrganisationRelationViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\opencase_entities\Form\OCOrganisationRelationForm",
 *       "add" = "Drupal\opencase_entities\Form\OCOrganisationRelationForm",
 *       "edit" = "Drupal\opencase_entities\Form\OCOrganisationRelationForm",
 *       "delete" = "Drupal\opencase_entities\Form\OCOrganisationRelationDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\opencase_entities\OCOrganisationRelationHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\opencase_entities\OCOrganisationRelationAccessControlHandler",
 *   },
 *   base_table = "oc_organisation_relation",
 *   translatable = FALSE,
 *   admin_permission = "administer organisation relation entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/opencase/oc_organisation_relation/{oc_organisation_relation}",
 *     "add-page" = "/opencase/oc_organisation_relation/add",
 *     "add-form" = "/opencase/oc_organisation_relation/add/{oc_organisation_relation_type}",
 *     "edit-form" = "/opencase/oc_organisation_relation/{oc_organisation_relation}/edit",
 *     "delete-form" = "/opencase/oc_organisation_relation/{oc_organisation_relation}/delete",
 *     "collection" = "/opencase/oc_organisation_relation",
 *   },
 *   bundle_entity_type = "oc_organisation_relation_type",
 *   field_ui_base_route = "entity.oc_organisation_relation_type.edit_form"
 * )
 */
class OCOrganisationRelation extends ContentEntityBase implements OCOrganisationRelationInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Organisation Relation entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('Link')
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['organisations'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Organisation to Link To'))
      ->setSetting('target_type', 'oc_organisation')
      ->setSetting('handler', 'default')
      ->setCardinality(2)
      ->setDisplayOptions('form', [
        'label' => 'above',
        'type' => 'entity_reference_autocomplete',
        'weight' => -2,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDefaultValueCallback('\Drupal\opencase_entities\Entity\OCOrganisationRelation::defaultValueCallback')
      ->setRequired(TRUE);
    return $fields;
  }
  static function defaultValueCallback() {
    return [\Drupal::request()->query->get('organisation_id')];
  }
}


