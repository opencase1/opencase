<?php

namespace Drupal\opencase_entities\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Bank Account entities.
 *
 * @ingroup opencase_entities
 */
interface OCBankAccountInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Bank Account name.
   *
   * @return string
   *   Name of the Bank Account.
   */
  public function getName();

  /**
   * Sets the Bank Account name.
   *
   * @param string $name
   *   The Bank Account name.
   *
   * @return \Drupal\opencase_entities\Entity\OCBankAccountInterface
   *   The called Bank Account entity.
   */
  public function setName($name);

  /**
   * Gets the Bank Account creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Bank Account.
   */
  public function getCreatedTime();

  /**
   * Sets the Bank Account creation timestamp.
   *
   * @param int $timestamp
   *   The Bank Account creation timestamp.
   *
   * @return \Drupal\opencase_entities\Entity\OCBankAccountInterface
   *   The called Bank Account entity.
   */
  public function setCreatedTime($timestamp);

}
