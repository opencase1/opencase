<?php

namespace Drupal\opencase_entities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Organisation Relation type entities.
 */
interface OCOrganisationRelationTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
