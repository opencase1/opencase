<?php

namespace Drupal\opencase_entities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Organisation type entity.
 *
 * @ConfigEntityType(
 *   id = "oc_organisation_type",
 *   label = @Translation("Organisation type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\opencase_entities\OCOrganisationTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\opencase_entities\Form\OCOrganisationTypeForm",
 *       "edit" = "Drupal\opencase_entities\Form\OCOrganisationTypeForm",
 *       "delete" = "Drupal\opencase_entities\Form\OCOrganisationTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\opencase_entities\OCOrganisationTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "oc_organisation_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "oc_organisation",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/opencase/oc_organisation_type/{oc_organisation_type}",
 *     "add-form" = "/admin/opencase/oc_organisation_type/add",
 *     "edit-form" = "/admin/opencase/oc_organisation_type/{oc_organisation_type}/edit",
 *     "delete-form" = "/admin/opencase/oc_organisation_type/{oc_organisation_type}/delete",
 *     "collection" = "/admin/opencase/oc_organisation_type"
 *   }
 * )
 */
class OCOrganisationType extends ConfigEntityBundleBase implements OCOrganisationTypeInterface {

  /**
   * The Organisation type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Organisation type label.
   *
   * @var string
   */
  protected $label;

}
