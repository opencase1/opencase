<?php

namespace Drupal\opencase_entities\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Equal Opps entity.
 *
 * @ingroup opencase_entities
 *
 * @ContentEntityType(
 *   id = "oc_equal_opps",
 *   label = @Translation("Equal Opps"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\opencase_entities\OCEqualOppsListBuilder",
 *     "views_data" = "Drupal\opencase_entities\Entity\OCEqualOppsViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\opencase_entities\Form\OCEqualOppsForm",
 *       "add" = "Drupal\opencase_entities\Form\OCEqualOppsForm",
 *       "edit" = "Drupal\opencase_entities\Form\OCEqualOppsForm",
 *       "delete" = "Drupal\opencase_entities\Form\OCEqualOppsDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\opencase_entities\OCEqualOppsHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\opencase_entities\OCEqualOppsAccessControlHandler",
 *   },
 *   base_table = "oc_equal_opps",
 *   translatable = FALSE,
 *   admin_permission = "administer equal opps entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/opencase/oc_equal_opps/{oc_equal_opps}",
 *     "add-form" = "/opencase/oc_equal_opps/add",
 *     "edit-form" = "/opencase/oc_equal_opps/{oc_equal_opps}/edit",
 *     "delete-form" = "/opencase/oc_equal_opps/{oc_equal_opps}/delete",
 *     "collection" = "/opencase/oc_equal_opps",
 *   },
 *   field_ui_base_route = "oc_equal_opps.settings"
 * )
 */
class OCEqualOpps extends ContentEntityBase implements OCEqualOppsInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    // get the name of the person and make that the name of the thing.
    $name = $this->get('oc_actor')->entity->get('name')->first()->value;
    $this->setName($name);
  }
  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Equal Opps entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);

    $fields['oc_actor'] = \Drupal\Core\Field\BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Person'))
      ->setDescription(t('The person this pertains to.'))
      ->setSetting('target_type', 'oc_actor')
      ->setSetting('handler', 'default')
      ->setCardinality(1)
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setRequired(TRUE);

    $fields['age'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Age'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'allowed_values' => [
          '18-24' => '18-24',
          '25-34' => '25-34',
          '35-44' => '35-44',
          '45-54' => '45-54',
          '56-70' => '56-70',
          '70+' => '70+',
        ],
      ])
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);
    $fields['access_needs'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Access Needs'))
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);
    $fields['gender'] = BaseFieldDefinition::create('list_string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Gender'))
      ->setSettings([
        'allowed_values' => [
          'man' => 'Man',
          'woman' => 'Woman',
          'non-binary' => 'Non-Binary',
          'trans-gnc' => 'Trans / GNC',
          'trans-man' => 'Trans man',
          'trans-woman' => 'Trans woman',
          'trans-nb' => 'Trans NB',
        ],
      ])
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);
    $fields['race_ethnicity'] = BaseFieldDefinition::create('list_string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Race/Ethnicity'))
      ->setSettings([
        'allowed_values' => [
          'white1' => 'White: English, Welsh, Scottish, Northern Irish or British',
          'white2' => 'White: Irish',
          'white3' => 'White: Gypsy or Irish Traveller',
          'white4' => 'White: Any other White background',
          'mixed1' => 'Mixed: White and Black Caribbean',
          'mixed2' => 'Mixed: White and Black African',
          'mixed3' => 'Mixed: White and Asian',
          'mixed4' => 'Mixed: Other',
          'asian1' => 'Asian: Indian',
          'asian2' => 'Asian: Bangladeshi',
          'asian3' => 'Asian: Pakistani',
          'asian4' => 'Asian: Chinese',
          'asian5' => 'Asian: Other',
          'black1' => 'Black: African',
          'black2' => 'Black: Caribbean',
          'black3' => 'Black: Other',
          'arab' => 'Arab',
          'other' => 'Other',
        ],
      ])
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);
    $fields['sexuality'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Sexuality'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 20,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);

    $fields['status']->setDescription(t('A boolean indicating whether the Equal Opps is published.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
