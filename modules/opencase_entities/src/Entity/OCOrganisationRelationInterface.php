<?php

namespace Drupal\opencase_entities\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Organisation Relation entities.
 *
 * @ingroup opencase_entities
 */
interface OCOrganisationRelationInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Organisation Relation name.
   *
   * @return string
   *   Name of the Organisation Relation.
   */
  public function getName();

  /**
   * Sets the Organisation Relation name.
   *
   * @param string $name
   *   The Organisation Relation name.
   *
   * @return \Drupal\opencase_entities\Entity\OCOrganisationRelationInterface
   *   The called Organisation Relation entity.
   */
  public function setName($name);

  /**
   * Gets the Organisation Relation creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Organisation Relation.
   */
  public function getCreatedTime();

  /**
   * Sets the Organisation Relation creation timestamp.
   *
   * @param int $timestamp
   *   The Organisation Relation creation timestamp.
   *
   * @return \Drupal\opencase_entities\Entity\OCOrganisationRelationInterface
   *   The called Organisation Relation entity.
   */
  public function setCreatedTime($timestamp);

}
