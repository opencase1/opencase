<?php

namespace Drupal\opencase_entities\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Organisation entities.
 *
 * @ingroup opencase_entities
 */
interface OCOrganisationInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Organisation name.
   *
   * @return string
   *   Name of the Organisation.
   */
  public function getName();

  /**
   * Sets the Organisation name.
   *
   * @param string $name
   *   The Organisation name.
   *
   * @return \Drupal\opencase_entities\Entity\OCOrganisationInterface
   *   The called Organisation entity.
   */
  public function setName($name);

  /**
   * Gets the Organisation creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Organisation.
   */
  public function getCreatedTime();

  /**
   * Sets the Organisation creation timestamp.
   *
   * @param int $timestamp
   *   The Organisation creation timestamp.
   *
   * @return \Drupal\opencase_entities\Entity\OCOrganisationInterface
   *   The called Organisation entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Organisation revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Organisation revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\opencase_entities\Entity\OCOrganisationInterface
   *   The called Organisation entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Organisation revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Organisation revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\opencase_entities\Entity\OCOrganisationInterface
   *   The called Organisation entity.
   */
  public function setRevisionUserId($uid);

}
