<?php

namespace Drupal\opencase_entities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Event type entities.
 */
interface OCEventTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
