<?php

namespace Drupal\opencase_entities\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines the Event entity.
 *
 * @ingroup opencase_entities
 *
 * @ContentEntityType(
 *   id = "oc_event",
 *   label = @Translation("Event"),
 *   bundle_label = @Translation("Event type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\opencase_entities\OCEventListBuilder",
 *     "views_data" = "Drupal\opencase_entities\Entity\OCEventViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\opencase_entities\Form\OCEventForm",
 *       "add" = "Drupal\opencase_entities\Form\OCEventForm",
 *       "edit" = "Drupal\opencase_entities\Form\OCEventForm",
 *       "delete" = "Drupal\opencase_entities\Form\OCEventDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\opencase_entities\OCEventHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\opencase_entities\OCEventAccessControlHandler",
 *   },
 *   base_table = "oc_event",
 *   translatable = FALSE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer event entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/opencase/oc_event/{oc_event}",
 *     "add-page" = "/opencase/oc_event/add",
 *     "add-form" = "/opencase/oc_event/add/{oc_event_type}",
 *     "edit-form" = "/opencase/oc_event/{oc_event}/edit",
 *     "delete-form" = "/opencase/oc_event/{oc_event}/delete",
 *     "collection" = "/opencase/oc_event",
 *   },
 *   bundle_entity_type = "oc_event_type",
 *   field_ui_base_route = "entity.oc_event_type.edit_form"
 * )
 */
class OCEvent extends ContentEntityBase implements OCEventInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }
  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entered by'))
      ->setDescription(t('The user ID of author of the Event entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Event entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Visible'))
      ->setDescription('If this box is not ticked this record will be hidden from view for most users. Users with access to unpublished entities will be able to restore it if needed.')
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setDefaultValue(TRUE);



    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['activity_date_time'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Date and time'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      // Uses the currentDateTime function from the Activity entity
      ->setDefaultValueCallback('\Drupal\opencase_entities\Entity\OCActivity::currentDateTime')
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setRequired(FALSE);

    $fields['attendees'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Attendees'))
      ->setDescription(t('People attending this event.'))
      ->setSetting('target_type', 'oc_actor')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setRequired(FALSE);


    return $fields;
  }

}
