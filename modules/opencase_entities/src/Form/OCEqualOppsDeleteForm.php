<?php

namespace Drupal\opencase_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Equal Opps entities.
 *
 * @ingroup opencase_entities
 */
class OCEqualOppsDeleteForm extends ContentEntityDeleteForm {


}
