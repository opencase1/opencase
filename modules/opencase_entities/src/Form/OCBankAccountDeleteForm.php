<?php

namespace Drupal\opencase_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Bank Account entities.
 *
 * @ingroup opencase_entities
 */
class OCBankAccountDeleteForm extends ContentEntityDeleteForm {


}
