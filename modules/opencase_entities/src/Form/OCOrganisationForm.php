<?php

namespace Drupal\opencase_entities\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Organisation edit forms.
 *
 * @ingroup opencase_entities
 */
class OCOrganisationForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\opencase_entities\Entity\OCOrganisation $entity */
    $form = parent::buildForm($form, $form_state);

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $entity->setNewRevision();
    $entity->setRevisionCreationTime(REQUEST_TIME);
    $entity->setRevisionUserId(\Drupal::currentUser()->id());

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Organisation.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Organisation.', [
          '%label' => $entity->label(),
        ]));
    }
    // If you have unpublished the entity and you can't see unpublished entities, redirect to a more informative message than just "Access Denied".
    if (!$form_state->getValue('status')['value'] && !\Drupal::currentUser()->hasPermission('view unpublished organisation entities')) {
      \Drupal::messenger()->addMessage($this->t('The record for "%label" is now unpublished & hidden from you.', [
        '%label' => $entity->label(),
      ]));
      $form_state->setRedirect('<front>');
    } else {
      $form_state->setRedirect('entity.oc_organisation.canonical', ['oc_organisation' => $entity->id()]);
    }
  }

}
