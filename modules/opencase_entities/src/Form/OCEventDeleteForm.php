<?php

namespace Drupal\opencase_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Event entities.
 *
 * @ingroup opencase_entities
 */
class OCEventDeleteForm extends ContentEntityDeleteForm {


}
