<?php

namespace Drupal\opencase_entities\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Activity edit forms.
 *
 * @ingroup opencase_entities
 */
class OCActivityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\opencase_entities\Entity\OCActivity */
    $form = parent::buildForm($form, $form_state);

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $entity->setNewRevision();
    $entity->setRevisionCreationTime(REQUEST_TIME);
    $entity->setRevisionUserId(\Drupal::currentUser()->id());

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
         \Drupal::messenger()->addMessage($this->t('Created the %bundle.', [
          '%bundle' => $entity->bundle(),
        ]));
        break;

      default:
         \Drupal::messenger()->addMessage($this->t('Saved the %bundle.', [
          '%bundle' => $entity->bundle(),
        ]));
    }
    // If you have unpublished the entity and you can't see unpublished entities, redirect to a more informative message than just "Access Denied".
    if (is_array($form_state->getValue('status')) && $form_state->getValue('status')['value'] == false && !\Drupal::currentUser()->hasPermission('view unpublished actor entities')) {
      \Drupal::messenger()->addMessage($this->t('The %bundle is now unpublished & hidden from you.', [
        '%bundle' => $entity->bundle(),
      ]));
      $form_state->setRedirect('<front>');
    } else {
      $form_state->setRedirect('entity.oc_activity.canonical', ['oc_activity' => $entity->id()]);
    }
  }

}
