<?php

namespace Drupal\opencase_entities\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Organisation revision.
 *
 * @ingroup opencase_entities
 */
class OCOrganisationRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Organisation revision.
   *
   * @var \Drupal\opencase_entities\Entity\OCOrganisationInterface
   */
  protected $revision;

  /**
   * The Organisation storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $oCOrganisationStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->oCOrganisationStorage = $container->get('entity_type.manager')->getStorage('oc_organisation');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oc_organisation_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.oc_organisation.version_history', ['oc_organisation' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $oc_organisation_revision = NULL) {
    $this->revision = $this->OCOrganisationStorage->loadRevision($oc_organisation_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->OCOrganisationStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Organisation: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Organisation %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.oc_organisation.canonical',
       ['oc_organisation' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {oc_organisation_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.oc_organisation.version_history',
         ['oc_organisation' => $this->revision->id()]
      );
    }
  }

}
