<?php

namespace Drupal\opencase_entities\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class OCOrganisationRelationTypeForm.
 */
class OCOrganisationRelationTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $oc_organisation_relation_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $oc_organisation_relation_type->label(),
      '#description' => $this->t("Label for the Organisation Relation type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $oc_organisation_relation_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\opencase_entities\Entity\OCOrganisationRelationType::load',
      ],
      '#disabled' => !$oc_organisation_relation_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $oc_organisation_relation_type = $this->entity;
    $status = $oc_organisation_relation_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Organisation Relation type.', [
          '%label' => $oc_organisation_relation_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Organisation Relation type.', [
          '%label' => $oc_organisation_relation_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($oc_organisation_relation_type->toUrl('collection'));
  }

}
