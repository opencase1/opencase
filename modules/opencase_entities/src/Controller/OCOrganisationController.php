<?php

namespace Drupal\opencase_entities\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\opencase_entities\Entity\OCOrganisationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OCOrganisationController.
 *
 *  Returns responses for Organisation routes.
 */
class OCOrganisationController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Organisation revision.
   *
   * @param int $oc_organisation_revision
   *   The Organisation revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($oc_organisation_revision) {
    $oc_organisation = $this->entityTypeManager()->getStorage('oc_organisation')
      ->loadRevision($oc_organisation_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('oc_organisation');

    return $view_builder->view($oc_organisation);
  }

  /**
   * Page title callback for a Organisation revision.
   *
   * @param int $oc_organisation_revision
   *   The Organisation revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($oc_organisation_revision) {
    $oc_organisation = $this->entityTypeManager()->getStorage('oc_organisation')
      ->loadRevision($oc_organisation_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $oc_organisation->label(),
      '%date' => $this->dateFormatter->format($oc_organisation->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Organisation.
   *
   * @param \Drupal\opencase_entities\Entity\OCOrganisationInterface $oc_organisation
   *   A Organisation object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(OCOrganisationInterface $oc_organisation) {
    $account = $this->currentUser();
    $oc_organisation_storage = $this->entityTypeManager()->getStorage('oc_organisation');

    $langcode = $oc_organisation->language()->getId();
    $langname = $oc_organisation->language()->getName();
    $languages = $oc_organisation->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $oc_organisation->label()]) : $this->t('Revisions for %title', ['%title' => $oc_organisation->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all organisation revisions") || $account->hasPermission('administer organisation entities')));
    $delete_permission = (($account->hasPermission("delete all organisation revisions") || $account->hasPermission('administer organisation entities')));

    $rows = [];

    $vids = $oc_organisation_storage->revisionIds($oc_organisation);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\opencase_entities\OCOrganisationInterface $revision */
      $revision = $oc_organisation_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $oc_organisation->getRevisionId()) {
          $url = new Url('entity.oc_organisation.revision', [
            'oc_organisation' => $oc_organisation->id(),
            'oc_organisation_revision' => $vid,
          ]);
          $link = \Drupal\Core\Link::fromTextAndUrl($date,$url)->toString();
        }
        else {
          $link = $oc_organisation->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.oc_organisation.translation_revert', [
                'oc_organisation' => $oc_organisation->id(),
                'oc_organisation_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.oc_organisation.revision_revert', [
                'oc_organisation' => $oc_organisation->id(),
                'oc_organisation_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.oc_organisation.revision_delete', [
                'oc_organisation' => $oc_organisation->id(),
                'oc_organisation_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['oc_organisation_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
