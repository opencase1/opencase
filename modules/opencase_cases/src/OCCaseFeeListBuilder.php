<?php

namespace Drupal\opencase_cases;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Case Fee entities.
 *
 * @ingroup opencase_cases
 */
class OCCaseFeeListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Case Fee ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\opencase_cases\Entity\OCCaseFee $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.oc_case_fee.edit_form',
      ['oc_case_fee' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
