<?php

namespace Drupal\opencase_cases\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class OCCaseFeeTypeForm.
 */
class OCCaseFeeTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $oc_case_fee_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $oc_case_fee_type->label(),
      '#description' => $this->t("Label for the Case Fee type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $oc_case_fee_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\opencase_cases\Entity\OCCaseFeeType::load',
      ],
      '#disabled' => !$oc_case_fee_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $oc_case_fee_type = $this->entity;
    $status = $oc_case_fee_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Case Fee type.', [
          '%label' => $oc_case_fee_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Case Fee type.', [
          '%label' => $oc_case_fee_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($oc_case_fee_type->toUrl('collection'));
  }

}
