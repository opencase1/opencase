<?php

namespace Drupal\opencase_cases\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Case Fee revision.
 *
 * @ingroup opencase_cases
 */
class OCCaseFeeRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Case Fee revision.
   *
   * @var \Drupal\opencase_cases\Entity\OCCaseFeeInterface
   */
  protected $revision;

  /**
   * The Case Fee storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $oCCaseFeeStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->oCCaseFeeStorage = $container->get('entity_type.manager')->getStorage('oc_case_fee');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oc_case_fee_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.oc_case_fee.version_history', ['oc_case_fee' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $oc_case_fee_revision = NULL) {
    $this->revision = $this->OCCaseFeeStorage->loadRevision($oc_case_fee_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->OCCaseFeeStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Case Fee: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Case Fee %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.oc_case_fee.canonical',
       ['oc_case_fee' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {oc_case_fee_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.oc_case_fee.version_history',
         ['oc_case_fee' => $this->revision->id()]
      );
    }
  }

}
