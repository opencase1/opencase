<?php

namespace Drupal\opencase_cases\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Case Provision entities.
 *
 * @ingroup opencase_cases
 */
class OCCaseProvisionDeleteForm extends ContentEntityDeleteForm {


}
