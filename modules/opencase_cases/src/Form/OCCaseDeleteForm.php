<?php

namespace Drupal\opencase_cases\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Case entities.
 *
 * @ingroup opencase_cases
 */
class OCCaseDeleteForm extends ContentEntityDeleteForm {


}
