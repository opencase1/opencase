<?php

namespace Drupal\opencase_cases\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Case edit forms.
 *
 * @ingroup opencase_cases
 */
class OCCaseForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\opencase_cases\Entity\OCCase */
    $form = parent::buildForm($form, $form_state);

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $entity = $this->entity;
    $entity->setNewRevision();
    $entity->setRevisionCreationTime(REQUEST_TIME);
    $entity->setRevisionUserId(\Drupal::currentUser()->id());

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addMessage($this->t('Created the %label Case.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        \Drupal::messenger()->addMessage($this->t('Saved the %label Case.', [
          '%label' => $entity->label(),
        ]));
    }
    // If you have unpublished the entity and you can't see unpublished entities, redirect to a more informative message than just "Access Denied".
    if (!$this->isPublished($form_state) && !\Drupal::currentUser()->hasPermission('view unpublished case entities')) {
      \Drupal::messenger()->addMessage($this->t('The record for "%label" is now unpublished & hidden from you.', [
        '%label' => $entity->label(),
      ]));
      $form_state->setRedirect('<front>');
    } else {
      $form_state->setRedirect('entity.oc_case.canonical', ['oc_case' => $entity->id()]);
    }
  }

  private function isPublished(FormStateInterface $form_state): bool {
    if (is_null($form_state->getValue('status'))) {
      return false; // some entities have nothing set for the status in which case we want to treat them as published by default.
      // TODO why don't they??
    }
    return $form_state->getValue('status')['value'];
  }

}
