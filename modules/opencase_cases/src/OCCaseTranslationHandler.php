<?php

namespace Drupal\opencase_cases;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for oc_case.
 */
class OCCaseTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
