<?php

namespace Drupal\opencase_cases;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\opencase_cases\Entity\OCCaseFeeInterface;

/**
 * Defines the storage handler class for Case Fee entities.
 *
 * This extends the base storage class, adding required special handling for
 * Case Fee entities.
 *
 * @ingroup opencase_cases
 */
interface OCCaseFeeStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Case Fee revision IDs for a specific Case Fee.
   *
   * @param \Drupal\opencase_cases\Entity\OCCaseFeeInterface $entity
   *   The Case Fee entity.
   *
   * @return int[]
   *   Case Fee revision IDs (in ascending order).
   */
  public function revisionIds(OCCaseFeeInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Case Fee author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Case Fee revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\opencase_cases\Entity\OCCaseFeeInterface $entity
   *   The Case Fee entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(OCCaseFeeInterface $entity);

  /**
   * Unsets the language for all Case Fee with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
