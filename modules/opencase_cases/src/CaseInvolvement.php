<?php

namespace Drupal\opencase_cases;

class CaseInvolvement {

  public static function getLinkedActorId($account) {
    return \Drupal\user\Entity\User::load($account->id())->get('field_linked_opencase_actor')->target_id;
  }

  public static function userIsInvolved($account, $case) {
    $actorId = self::getLinkedActorId($account);        
    $query = \Drupal::entityQuery('oc_case_provision')
    ->accessCheck(FALSE)
    ->condition('oc_provider', $actorId)
    ->condition('oc_case', $case->id());
    $results = $query->execute();
    return !empty($results);
  }

  public static function userIsInvolved_activity($account, $activity) {
    $case_id = $activity->oc_case->target_id;
    $case = \Drupal::entityTypeManager()->getStorage('oc_case')->load($case_id);
    return self::userIsInvolved($account, $case);
  }
}
