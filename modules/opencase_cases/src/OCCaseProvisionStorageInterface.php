<?php

namespace Drupal\opencase_cases;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\opencase_cases\Entity\OCCaseProvisionInterface;

/**
 * Defines the storage handler class for Case Provision entities.
 *
 * This extends the base storage class, adding required special handling for
 * Case Provision entities.
 *
 * @ingroup opencase_cases
 */
interface OCCaseProvisionStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Case Provision revision IDs for a specific Case Provision.
   *
   * @param \Drupal\opencase_cases\Entity\OCCaseProvisionInterface $entity
   *   The Case Provision entity.
   *
   * @return int[]
   *   Case Provision revision IDs (in ascending order).
   */
  public function revisionIds(OCCaseProvisionInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Case Provision author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Case Provision revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\opencase_cases\Entity\OCCaseProvisionInterface $entity
   *   The Case Provision entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(OCCaseProvisionInterface $entity);

  /**
   * Unsets the language for all Case Provision with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
