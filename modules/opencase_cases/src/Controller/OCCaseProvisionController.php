<?php

namespace Drupal\opencase_cases\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\opencase_cases\Entity\OCCaseProvisionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OCCaseProvisionController.
 *
 *  Returns responses for Case Provision routes.
 */
class OCCaseProvisionController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Case Provision revision.
   *
   * @param int $oc_case_provision_revision
   *   The Case Provision revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($oc_case_provision_revision) {
    $oc_case_provision = $this->entityTypeManager()->getStorage('oc_case_provision')
      ->loadRevision($oc_case_provision_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('oc_case_provision');

    return $view_builder->view($oc_case_provision);
  }

  /**
   * Page title callback for a Case Provision revision.
   *
   * @param int $oc_case_provision_revision
   *   The Case Provision revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($oc_case_provision_revision) {
    $oc_case_provision = $this->entityTypeManager()->getStorage('oc_case_provision')
      ->loadRevision($oc_case_provision_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $oc_case_provision->label(),
      '%date' => $this->dateFormatter->format($oc_case_provision->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Case Provision.
   *
   * @param \Drupal\opencase_cases\Entity\OCCaseProvisionInterface $oc_case_provision
   *   A Case Provision object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(OCCaseProvisionInterface $oc_case_provision) {
    $account = $this->currentUser();
    $oc_case_provision_storage = $this->entityTypeManager()->getStorage('oc_case_provision');

    $langcode = $oc_case_provision->language()->getId();
    $langname = $oc_case_provision->language()->getName();
    $languages = $oc_case_provision->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $oc_case_provision->label()]) : $this->t('Revisions for %title', ['%title' => $oc_case_provision->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all case provision revisions") || $account->hasPermission('administer case provision entities')));
    $delete_permission = (($account->hasPermission("delete all case provision revisions") || $account->hasPermission('administer case provision entities')));

    $rows = [];

    $vids = $oc_case_provision_storage->revisionIds($oc_case_provision);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\opencase_cases\OCCaseProvisionInterface $revision */
      $revision = $oc_case_provision_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $oc_case_provision->getRevisionId()) {
          $url = new Url('entity.oc_case_provision.revision', [
            'oc_case_provision' => $oc_case_provision->id(),
            'oc_case_provision_revision' => $vid,
          ]);
          $link = \Drupal\Core\Link::fromTextAndUrl($date, $url)->toString();
        }
        else {
          $link = $oc_case_provision->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.oc_case_provision.translation_revert', [
                'oc_case_provision' => $oc_case_provision->id(),
                'oc_case_provision_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.oc_case_provision.revision_revert', [
                'oc_case_provision' => $oc_case_provision->id(),
                'oc_case_provision_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.oc_case_provision.revision_delete', [
                'oc_case_provision' => $oc_case_provision->id(),
                'oc_case_provision_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['oc_case_provision_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
