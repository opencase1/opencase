<?php

namespace Drupal\opencase_cases\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\opencase_cases\Entity\OCCaseFeeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OCCaseFeeController.
 *
 *  Returns responses for Case Fee routes.
 */
class OCCaseFeeController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Case Fee revision.
   *
   * @param int $oc_case_fee_revision
   *   The Case Fee revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($oc_case_fee_revision) {
    $oc_case_fee = $this->entityTypeManager()->getStorage('oc_case_fee')
      ->loadRevision($oc_case_fee_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('oc_case_fee');

    return $view_builder->view($oc_case_fee);
  }

  /**
   * Page title callback for a Case Fee revision.
   *
   * @param int $oc_case_fee_revision
   *   The Case Fee revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($oc_case_fee_revision) {
    $oc_case_fee = $this->entityTypeManager()->getStorage('oc_case_fee')
      ->loadRevision($oc_case_fee_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $oc_case_fee->label(),
      '%date' => $this->dateFormatter->format($oc_case_fee->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Case Fee.
   *
   * @param \Drupal\opencase_cases\Entity\OCCaseFeeInterface $oc_case_fee
   *   A Case Fee object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(OCCaseFeeInterface $oc_case_fee) {
    $account = $this->currentUser();
    $oc_case_fee_storage = $this->entityTypeManager()->getStorage('oc_case_fee');

    $langcode = $oc_case_fee->language()->getId();
    $langname = $oc_case_fee->language()->getName();
    $languages = $oc_case_fee->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $oc_case_fee->label()]) : $this->t('Revisions for %title', ['%title' => $oc_case_fee->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all case fee revisions") || $account->hasPermission('administer case fee entities')));
    $delete_permission = (($account->hasPermission("delete all case fee revisions") || $account->hasPermission('administer case fee entities')));

    $rows = [];

    $vids = $oc_case_fee_storage->revisionIds($oc_case_fee);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\opencase_cases\OCCaseFeeInterface $revision */
      $revision = $oc_case_fee_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $oc_case_fee->getRevisionId()) {
          $url = new Url('entity.oc_case_fee.revision', [
            'oc_case_fee' => $oc_case_fee->id(),
            'oc_case_fee_revision' => $vid,
          ]);
          $link = \Drupal\Core\Link::fromTextAndUrl($date, $url)->toString();
        }
        else {
          $link = $oc_case_fee->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.oc_case_fee.translation_revert', [
                'oc_case_fee' => $oc_case_fee->id(),
                'oc_case_fee_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.oc_case_fee.revision_revert', [
                'oc_case_fee' => $oc_case_fee->id(),
                'oc_case_fee_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.oc_case_fee.revision_delete', [
                'oc_case_fee' => $oc_case_fee->id(),
                'oc_case_fee_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['oc_case_fee_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
