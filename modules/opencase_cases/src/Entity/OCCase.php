<?php

namespace Drupal\opencase_cases\Entity;

use Drupal\user\UserInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\opencase_entities\Entity\OCActor;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\opencase_entities\Entity\OCOrganisation;

/**
 * Defines the Case entity.
 *
 * @ingroup opencase_cases
 *
 * @ContentEntityType(
 *   id = "oc_case",
 *   label = @Translation("Case"),
 *   bundle_label = @Translation("Case type"),
 *   handlers = {
 *     "storage" = "Drupal\opencase_cases\OCCaseStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\opencase_cases\OCCaseListBuilder",
 *     "views_data" = "Drupal\opencase_cases\Entity\OCCaseViewsData",
 *     "translation" = "Drupal\opencase_cases\OCCaseTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\opencase_cases\Form\OCCaseForm",
 *       "add" = "Drupal\opencase_cases\Form\OCCaseForm",
 *       "edit" = "Drupal\opencase_cases\Form\OCCaseForm",
 *       "delete" = "Drupal\opencase_cases\Form\OCCaseDeleteForm",
 *     },
 *     "access" = "Drupal\opencase_cases\OCCaseAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\opencase_cases\OCCaseHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "oc_case",
 *   data_table = "oc_case_field_data",
 *   revision_table = "oc_case_revision",
 *   revision_data_table = "oc_case_field_revision",
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log"
 *   },
 *   translatable = TRUE,
 *   admin_permission = "administer case entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/opencase/oc_case/{oc_case}",
 *     "add-page" = "/opencase/oc_case/add",
 *     "add-form" = "/opencase/oc_case/add/{oc_case_type}",
 *     "edit-form" = "/opencase/oc_case/{oc_case}/edit",
 *     "delete-form" = "/opencase/oc_case/{oc_case}/delete",
 *     "version-history" = "/opencase/oc_case/{oc_case}/revisions",
 *     "revision" = "/opencase/oc_case/{oc_case}/revisions/{oc_case_revision}/view",
 *     "revision_revert" = "/opencase/oc_case/{oc_case}/revisions/{oc_case_revision}/revert",
 *     "revision_delete" = "/opencase/oc_case/{oc_case}/revisions/{oc_case_revision}/delete",
 *     "translation_revert" = "/opencase/oc_case/{oc_case}/revisions/{oc_case_revision}/revert/{langcode}",
 *     "collection" = "/opencase/oc_case",
 *   },
 *   bundle_entity_type = "oc_case_type",
 *   field_ui_base_route = "entity.oc_case_type.edit_form"
 * )
 */
class OCCase extends RevisionableContentEntityBase implements OCCaseInterface
{

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */

  public static function loadFromUrlQueryParameter(string $param) {
    return self::load(\Drupal::request()->query->get($param));
  }
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values)
  {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  public function getCaseProviderIds(int $role_id = null): array {
    $case_provision_ids = $this->getCaseProvisionIds();
    $provider_ids = [];
    foreach($case_provision_ids as $id) {
      $provision = \Drupal::entityTypeManager()->getStorage('oc_case_provision')->load($id);
      if ($provision instanceOf OCCaseProvision) {
        if (is_null($role_id) || $role_id == $provision->get('oc_case_provider_role')->target_id)  {
          $provider_ids[] = $provision->get('oc_provider')->target_id;
        }
      }
    }
    return $provider_ids;
  }

  private function getCaseProvisionIds(): array {
    $query = \Drupal::entityTypeManager()->getStorage('oc_case_provision')->getQuery();
    $query->accessCheck(FALSE)->condition('oc_case.target_id', $this->id());
    return $query->execute();
  }

  public function deleteCaseProvisions(): void {
    $this->deleteChildren('oc_case_provision');
  }
  public function deleteActivities(): void {
    $this->deleteChildren('oc_activity');
  }

  private function deleteChildren($child_entity_type):void {
    $query = \Drupal::entityQuery($child_entity_type)
      ->accessCheck(FALSE)
      ->condition('oc_case.target_id', $this->id());
    $ids =  $query->execute();
    foreach($ids as $id) {
      \Drupal::entityTypeManager()
        ->getStorage($child_entity_type)
        ->load($id)->delete();
    }
  }

  public static function defaultTarget()
  {
    if (opencase_entities_get('target_id')) return [opencase_entities_get('target_id')];
    else return [];
  }

  // This can return null when a case target has been deleted and
  // the case intentionally preserved. 
  // TODO: understand why the entity can be null even if isEmpty() returns false
  public function getTargetEntity(): ?ContentEntityBase {
    if (!$this->oc_target->isEmpty()) {
      return $this->oc_target->entity;
    } elseif ($this->hasField('client') && !$this->client->isEmpty()) {
      return $this->client->entity;
    } else {
      throw new \Exception('No target found on case');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel)
  {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    } elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */

  // TODO this expects the client field which is only in GNO.
  public function getClient(): ?OCOrganisation {
    $case_id = $this->client->target_id;
    return OCOrganisation::load($case_id);
  }

  public function preSave(EntityStorageInterface $storage)
  {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the oc_case owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name)
  {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime()
  {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp)
  {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner()
  {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId()
  {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid)
  {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account)
  {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished()
  {
    return (bool)$this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published)
  {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  public function addToTotalFee(float $amountToAdd): void {
    $this->set('total_fee', $this->total_fee->value + $amountToAdd);
    $this->save();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields = parent::baseFieldDefinitions($entity_type);

    // not currently used. Will add form and view settings when ready
    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Visible'))
      ->setDescription('If this box is not ticked this record will be hidden from view for most users. Users with access to unpublished entities will be able to restore it if needed.')
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setDefaultValue(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Created by'))
      ->setDescription(t('The user ID of author of the Case entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);


    $fields['oc_target'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Target'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'oc_actor')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('\Drupal\opencase_cases\Entity\OCCase::defaultTarget')
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 120,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setRequired(TRUE);


    $fields['files'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Files'))
      ->setDescription(t('Files attached to this case'))
      ->setSetting('file_directory', '[date:custom:Y]-[date:custom:m]')
      ->setSetting('handler', 'default:file')
      ->setSetting('file_extensions', 'txt jpg jpeg gif rtf xls xlsx doc swf png pdf docx csv')
      ->setSetting('description_field', 'true')
      ->setSetting('uri_scheme', 'private')
      ->setCardinality(-1)
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created on'))
      ->setDescription(t('When the case was created.'))
      ->setDisplayConfigurable('view', true);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Last updated'))
      ->setDescription(t('When the case was last edited.'))
      ->setDisplayConfigurable('view', true);

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['notes'] = BaseFieldDefinition::create('string_long')
      ->setRevisionable(TRUE)
      ->setLabel(t('Notes'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);


    $fields['total_fee'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Total Fee'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'prefix' => '£',
      ])
      ->setDisplayConfigurable('view', true);

    return $fields;
  }
}
