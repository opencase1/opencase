<?php

namespace Drupal\opencase_cases\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Case Provision entities.
 *
 * @ingroup opencase_cases
 */
interface OCCaseProvisionInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Case Provision name.
   *
   * @return string
   *   Name of the Case Provision.
   */
  public function getName();

  /**
   * Sets the Case Provision name.
   *
   * @param string $name
   *   The Case Provision name.
   *
   * @return \Drupal\opencase_cases\Entity\OCCaseProvisionInterface
   *   The called Case Provision entity.
   */
  public function setName($name);

  /**
   * Gets the Case Provision creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Case Provision.
   */
  public function getCreatedTime();

  /**
   * Sets the Case Provision creation timestamp.
   *
   * @param int $timestamp
   *   The Case Provision creation timestamp.
   *
   * @return \Drupal\opencase_cases\Entity\OCCaseProvisionInterface
   *   The called Case Provision entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Case Provision revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Case Provision revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\opencase_cases\Entity\OCCaseProvisionInterface
   *   The called Case Provision entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Case Provision revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Case Provision revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\opencase_cases\Entity\OCCaseProvisionInterface
   *   The called Case Provision entity.
   */
  public function setRevisionUserId($uid);

}
