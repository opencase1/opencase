<?php

namespace Drupal\opencase_cases\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Case Fee type entity.
 *
 * @ConfigEntityType(
 *   id = "oc_case_fee_type",
 *   label = @Translation("Case Fee type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\opencase_cases\OCCaseFeeTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\opencase_cases\Form\OCCaseFeeTypeForm",
 *       "edit" = "Drupal\opencase_cases\Form\OCCaseFeeTypeForm",
 *       "delete" = "Drupal\opencase_cases\Form\OCCaseFeeTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\opencase_cases\OCCaseFeeTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "oc_case_fee_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "oc_case_fee",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/opencase/oc_case_fee_type/{oc_case_fee_type}",
 *     "add-form" = "/opencase/oc_case_fee_type/add",
 *     "edit-form" = "/opencase/oc_case_fee_type/{oc_case_fee_type}/edit",
 *     "delete-form" = "/opencase/oc_case_fee_type/{oc_case_fee_type}/delete",
 *     "collection" = "/opencase/oc_case_fee_type"
 *   }
 * )
 */
class OCCaseFeeType extends ConfigEntityBundleBase implements OCCaseFeeTypeInterface {

  /**
   * The Case Fee type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Case Fee type label.
   *
   * @var string
   */
  protected $label;

}
