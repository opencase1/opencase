<?php

namespace Drupal\opencase_cases\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Case Provision type entities.
 */
interface OCCaseProvisionTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
