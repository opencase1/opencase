<?php

namespace Drupal\opencase_cases\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Case Fee type entities.
 */
interface OCCaseFeeTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
