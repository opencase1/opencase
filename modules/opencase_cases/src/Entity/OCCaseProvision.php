<?php

namespace Drupal\opencase_cases\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Case Provision entity.
 *
 * @ingroup opencase_cases
 *
 * @ContentEntityType(
 *   id = "oc_case_provision",
 *   label = @Translation("Case Provision"),
 *   bundle_label = @Translation("Case Provision type"),
 *   handlers = {
 *     "storage" = "Drupal\opencase_cases\OCCaseProvisionStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\opencase_cases\OCCaseProvisionListBuilder",
 *     "views_data" = "Drupal\opencase_cases\Entity\OCCaseProvisionViewsData",
 *     "translation" = "Drupal\opencase_cases\OCCaseProvisionTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\opencase_cases\Form\OCCaseProvisionForm",
 *       "add" = "Drupal\opencase_cases\Form\OCCaseProvisionForm",
 *       "edit" = "Drupal\opencase_cases\Form\OCCaseProvisionForm",
 *       "delete" = "Drupal\opencase_cases\Form\OCCaseProvisionDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\opencase_cases\OCCaseProvisionHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\opencase_cases\OCCaseProvisionAccessControlHandler",
 *   },
 *   base_table = "oc_case_provision",
 *   data_table = "oc_case_provision_field_data",
 *   revision_table = "oc_case_provision_revision",
 *   revision_data_table = "oc_case_provision_field_revision",
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer case provision entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "canonical" = "/opencase/oc_case_provision/{oc_case_provision}",
 *     "add-page" = "/opencase/oc_case_provision/add",
 *     "add-form" = "/opencase/oc_case_provision/add/{oc_case_provision_type}",
 *     "edit-form" = "/opencase/oc_case_provision/{oc_case_provision}/edit",
 *     "delete-form" = "/opencase/oc_case_provision/{oc_case_provision}/delete",
 *     "version-history" = "/opencase/oc_case_provision/{oc_case_provision}/revisions",
 *     "revision" = "/opencase/oc_case_provision/{oc_case_provision}/revisions/{oc_case_provision_revision}/view",
 *     "revision_revert" = "/opencase/oc_case_provision/{oc_case_provision}/revisions/{oc_case_provision_revision}/revert",
 *     "revision_delete" = "/opencase/oc_case_provision/{oc_case_provision}/revisions/{oc_case_provision_revision}/delete",
 *     "translation_revert" = "/opencase/oc_case_provision/{oc_case_provision}/revisions/{oc_case_provision_revision}/revert/{langcode}",
 *     "collection" = "/opencase/oc_case_provision",
 *   },
 *   bundle_entity_type = "oc_case_provision_type",
 *   field_ui_base_route = "entity.oc_case_provision_type.edit_form"
 * )
 */
class OCCaseProvision extends EditorialContentEntityBase implements OCCaseProvisionInterface
{

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values)
  {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel)
  {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    } elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage)
  {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the oc_case_provision owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name)
  {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime()
  {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp)
  {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner()
  {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId()
  {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid)
  {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account)
  {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Case Provision entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Case Provision entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);


    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Visible'))
      ->setDescription('If this box is not ticked this record will be hidden from view for most users. Users with access to unpublished entities will be able to restore it if needed.')
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true)
      ->setDefaultValue(TRUE);


    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['oc_case'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Case'))
      ->setSetting('target_type', 'oc_case')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setCardinality(1)
      ->setDefaultValueCallback('opencase_cases_default_case_id')  // defined in opencase_cases.module
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['oc_provider'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Provider'))
      ->setSetting('target_type', 'oc_actor') // TODO: this should eventually point to a Provider rather than an Actor
      ->setSetting('handler', 'views')
      ->setTranslatable(TRUE)
      ->setCardinality(1)
      ->setSetting('handler_settings', [
        'view' => [
          'view_name' => 'case_providers',
          'display_name' => 'entity_reference_1',
          'arguments' => [],
        ]
      ])
      ->setDisplayConfigurable("view", true)
      ->setDisplayConfigurable("form", true)
      ->setRequired(TRUE);

    $fields['oc_case_provider_role'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Role'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['oc_case_provider_role' => 'oc_case_provider_role']])
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setCardinality(1)
      ->setDisplayConfigurable("view", true)
      ->setDisplayConfigurable("form", true)
      ->setRequired(FALSE);
    return $fields;
  }

}
