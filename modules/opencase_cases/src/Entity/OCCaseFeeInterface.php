<?php

namespace Drupal\opencase_cases\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Case Fee entities.
 *
 * @ingroup opencase_cases
 */
interface OCCaseFeeInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Case Fee name.
   *
   * @return string
   *   Name of the Case Fee.
   */
  public function getName();

  /**
   * Sets the Case Fee name.
   *
   * @param string $name
   *   The Case Fee name.
   *
   * @return \Drupal\opencase_cases\Entity\OCCaseFeeInterface
   *   The called Case Fee entity.
   */
  public function setName($name);

  /**
   * Gets the Case Fee creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Case Fee.
   */
  public function getCreatedTime();

  /**
   * Sets the Case Fee creation timestamp.
   *
   * @param int $timestamp
   *   The Case Fee creation timestamp.
   *
   * @return \Drupal\opencase_cases\Entity\OCCaseFeeInterface
   *   The called Case Fee entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Case Fee revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Case Fee revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\opencase_cases\Entity\OCCaseFeeInterface
   *   The called Case Fee entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Case Fee revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Case Fee revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\opencase_cases\Entity\OCCaseFeeInterface
   *   The called Case Fee entity.
   */
  public function setRevisionUserId($uid);

}
