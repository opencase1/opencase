<?php

namespace Drupal\opencase_cases\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Case Fee entity.
 *
 * @ingroup opencase_cases
 *
 * @ContentEntityType(
 *   id = "oc_case_fee",
 *   label = @Translation("Case Fee"),
 *   bundle_label = @Translation("Case Fee type"),
 *   handlers = {
 *     "storage" = "Drupal\opencase_cases\OCCaseFeeStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\opencase_cases\OCCaseFeeListBuilder",
 *     "views_data" = "Drupal\opencase_cases\Entity\OCCaseFeeViewsData",
 *     "translation" = "Drupal\opencase_cases\OCCaseFeeTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\opencase_cases\Form\OCCaseFeeForm",
 *       "add" = "Drupal\opencase_cases\Form\OCCaseFeeForm",
 *       "edit" = "Drupal\opencase_cases\Form\OCCaseFeeForm",
 *       "delete" = "Drupal\opencase_cases\Form\OCCaseFeeDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\opencase_cases\OCCaseFeeHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\opencase_cases\OCCaseFeeAccessControlHandler",
 *   },
 *   base_table = "oc_case_fee",
 *   data_table = "oc_case_fee_field_data",
 *   revision_table = "oc_case_fee_revision",
 *   revision_data_table = "oc_case_fee_field_revision",
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer case fee entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "canonical" = "/opencase/oc_case_fee/{oc_case_fee}",
 *     "add-page" = "/opencase/oc_case_fee/add",
 *     "add-form" = "/opencase/oc_case_fee/add/{oc_case_fee_type}",
 *     "edit-form" = "/opencase/oc_case_fee/{oc_case_fee}/edit",
 *     "delete-form" = "/opencase/oc_case_fee/{oc_case_fee}/delete",
 *     "version-history" = "/opencase/oc_case_fee/{oc_case_fee}/revisions",
 *     "revision" = "/opencase/oc_case_fee/{oc_case_fee}/revisions/{oc_case_fee_revision}/view",
 *     "revision_revert" = "/opencase/oc_case_fee/{oc_case_fee}/revisions/{oc_case_fee_revision}/revert",
 *     "revision_delete" = "/opencase/oc_case_fee/{oc_case_fee}/revisions/{oc_case_fee_revision}/delete",
 *     "translation_revert" = "/opencase/oc_case_fee/{oc_case_fee}/revisions/{oc_case_fee_revision}/revert/{langcode}",
 *     "collection" = "/opencase/oc_case_fee",
 *   },
 *   bundle_entity_type = "oc_case_fee_type",
 *   field_ui_base_route = "entity.oc_case_fee_type.edit_form"
 * )
 */
class OCCaseFee extends EditorialContentEntityBase implements OCCaseFeeInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the oc_case_fee owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  public function getCase():\Drupal\opencase_cases\Entity\OCCase {
    return \Drupal\opencase_cases\Entity\OCCase::load($this->oc_case->target_id);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Case Fee entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Case Fee entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Visible'))
      ->setDescription(t('A boolean indicating whether the Case Fee is published.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['oc_case'] = \Drupal\Core\Field\BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Case'))
      ->setSetting('target_type', 'oc_case')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setCardinality(1)
      ->setDefaultValueCallback('opencase_cases_default_case_id')  // defined in opencase_cases.module
      ->setDisplayConfigurable("view", true)
      ->setDisplayConfigurable("form", true)
      ->setRequired(TRUE);

    $fields['oc_fee_category'] = \Drupal\Core\Field\BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Fee Category'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['oc_fee_category' => 'oc_fee_category']])
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setCardinality(1)
      ->setDisplayConfigurable("view", true)
      ->setDisplayConfigurable("form", true)
      ->setRequired(FALSE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setRevisionable(TRUE)
      ->setLabel(t('Description'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);

    $fields['amount'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Amount'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'prefix' => '£',
      ])
      ->setRequired(TRUE)
      ->SetDisplayConfigurable("form", true)
      ->SetDisplayConfigurable("view", true);

    return $fields;
  }

}
