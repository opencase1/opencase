<?php

namespace Drupal\opencase_cases\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Case Fee entities.
 */
class OCCaseFeeViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
