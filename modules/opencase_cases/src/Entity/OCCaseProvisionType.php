<?php

namespace Drupal\opencase_cases\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Case Provision type entity.
 *
 * @ConfigEntityType(
 *   id = "oc_case_provision_type",
 *   label = @Translation("Case Provision type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\opencase_cases\OCCaseProvisionTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\opencase_cases\Form\OCCaseProvisionTypeForm",
 *       "edit" = "Drupal\opencase_cases\Form\OCCaseProvisionTypeForm",
 *       "delete" = "Drupal\opencase_cases\Form\OCCaseProvisionTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\opencase_cases\OCCaseProvisionTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "oc_case_provision_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "oc_case_provision",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/opencase/oc_case_provision_type/{oc_case_provision_type}",
 *     "add-form" = "/opencase/oc_case_provision_type/add",
 *     "edit-form" = "/opencase/oc_case_provision_type/{oc_case_provision_type}/edit",
 *     "delete-form" = "/opencase/oc_case_provision_type/{oc_case_provision_type}/delete",
 *     "collection" = "/opencase/oc_case_provision_type"
 *   }
 * )
 */
class OCCaseProvisionType extends ConfigEntityBundleBase implements OCCaseProvisionTypeInterface {

  /**
   * The Case Provision type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Case Provision type label.
   *
   * @var string
   */
  protected $label;

}
