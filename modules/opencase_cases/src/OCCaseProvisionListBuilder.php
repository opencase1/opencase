<?php

namespace Drupal\opencase_cases;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Case Provision entities.
 *
 * @ingroup opencase_cases
 */
class OCCaseProvisionListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Case Provision ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\opencase_cases\Entity\OCCaseProvision $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.oc_case_provision.edit_form',
      ['oc_case_provision' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
