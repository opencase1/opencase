<?php

/**
 * @file
 * Contains oc_case_fee.page.inc.
 *
 * Page callback for Case Fee entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Case Fee templates.
 *
 * Default template: oc_case_fee.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_oc_case_fee(array &$variables) {
  // Fetch OCCaseFee Entity Object.
  $oc_case_fee = $variables['elements']['#oc_case_fee'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
