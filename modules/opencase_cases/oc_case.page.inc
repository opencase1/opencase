<?php

/**
 * @file
 * Contains oc_case.page.inc.
 *
 * Page callback for Case entities.
 */

use Drupal\Core\Render\Element;
use Drupal\opencase;

/**
 * Prepares variables for Case templates.
 *
 * Default template: oc_case.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_oc_case(array &$variables) {
  _template_preprocess_entity($variables);
}

