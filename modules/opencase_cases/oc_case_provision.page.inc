<?php

/**
 * @file
 * Contains oc_case_provision.page.inc.
 *
 * Page callback for Case Provision entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Case Provision templates.
 *
 * Default template: oc_case_provision.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_oc_case_provision(array &$variables) {
  // Fetch OCCaseProvision Entity Object.
  $oc_case_provision = $variables['elements']['#oc_case_provision'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
